/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.database.ConnectException;
import de.bender_dv.ardgate.pords2pojo.ARCN0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.OutConnectFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.jvagate.communication.Context;

/**
 * @author Bender
 *
 */
class ARCN0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(ARCN0100Handler.class);

	/**
	 * 
	 */
	ARCN0100Handler() {
		super();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		
		ARCN0100Format in = new ARCN0100Format(inFormat, getCcsid());
		OutConnectFormat of = new OutConnectFormat(in.getUserId());
		if((actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup())) == null){
			actGroup = new ActivationGroup(in.getActivationGroup());
			session.setAttribute("AG" + in.getActivationGroup(), actGroup);
		}
		ArdConnection con = new ArdConnection(in.getDatabaseName(), in.getActivationGroup());
		con.setCcsid(Context.getInstance().getCcsid());
		con.setLevel(in.getLevel());
		try {
			if(con.connect(in.getUserId(), in.getPassword())){
				actGroup.setAttribute(in.getConnectionKey(), con);
			}
			else{
				actGroup.removeAttribute(in.getConnectionKey());
			}
		} catch (ConnectException e) {
			log.error(e);
		}
		SqlcaBean sqlca = con.getSqlca();
		ArdOutFormat ao = new ArdOutFormat(sqlca, of);
		return ao;
	}
}
