/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.pords2pojo.ARCC0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
/**
 * @author hansguenther
 *
 */
class ARCC0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(ARCC0100Handler.class);
	private ARCC0100Format in;
	private ResultSet rs;
	private Statement stmt;
	/**
	 * 
	 */
	ARCC0100Handler() {
		super();
		sqlca = new SqlcaBean();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormat);
		in = new ARCC0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		sc = ardCon.getSqlstateConverter();
		rs = ardCon.getResultSet(in.getSectionNumber());
		if (rs != null) { 
			try {
				rs.close();
			} catch (SQLException e) {
				sqlca = new SqlcaBean(e, sc);
			}
		}
//		stmt = ardCon.getStatement(in.getSectionNumber());
//		if (stmt!= null) { 
//			try {
//				stmt.close();
//			} catch (SQLException e) {
//				sqlca = new SqlcaBean(e, sc);
//			}  // ToDO remove from ARDconnection
//		}
		return new ArdOutFormat(sqlca);
	}

}
