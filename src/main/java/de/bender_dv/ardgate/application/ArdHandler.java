/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.pords2pojo.ArdInFormat;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.jvagate.application.EventHandler;
import de.bender_dv.jvagate.communication.Request;
import de.bender_dv.jvagate.communication.Response;

public class ArdHandler implements EventHandler {
	private static final Log log = LogFactory.getLog(ArdHandler.class);
	private ArdInFormat in;
	private String formatName;
	public void performWork(Request req, Response res) {
		FormatHandler fh;
		byte[] data;
		ArdOutFormat ao;
		log.debug("Request: " + req);
		in = new ArdInFormat(req.getEventData());
		data = in.getInFormat();
		formatName = in.getInFormatName();
		log.info("Format: " + formatName + " ArdGate Version: " + Version.getVersion());
		fh = FormatHandlerFactory.getInstance().getFormatHandler(formatName, req.getSession());
		ao = fh.getOutFormat(data, formatName);
		res.setResultData(ao.getHuddle());
	}
}
