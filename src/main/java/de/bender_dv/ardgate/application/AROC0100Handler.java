/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.database.NotYetImplementedException;
import de.bender_dv.ardgate.pords2pojo.AROC0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.OutOpenCursorFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.ardgate.pords2pojo.SqldaBean;
import de.bender_dv.ardgate.pords2pojo.SqlvarBean;

/**
 * @author Bender
 *	TODO ?
 *	DECLARE CURSOR with SQL String sent to AOC0100 without prepare
 */
class AROC0100Handler extends FormatHandler {
	static final Log log = LogFactory.getLog(AROC0100Handler.class);
	private AROC0100Format in;
	private Connection con;
	private PreparedStatement stmt;
	ResultSet rs;
	private ArdOutFormat ao;
	private OutOpenCursorFormat of;
	protected ResultSetMetaData rsMetaData;
	protected SqldaBean sqlda; 
	/**
	 * 
	 */
	AROC0100Handler() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormat);
		in = new AROC0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		sc = ardCon.getSqlstateConverter();
		con = ardCon.getConnection();
		try {		
			if ((stmt = ardCon.getStatement(in.getSectionNumber())) == null){
				throw new NotYetImplementedException("Cursor not prepared",  "58033", -969);
			}
			execute();
			sqlca = new SqlcaBean();
			of = new OutOpenCursorFormat(sqlda);
			ao = new ArdOutFormat(sqlca, of);
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sc);
			sqlca.setSqlErrMc(e.getMessage());
			log.debug(e);
			ao = new ArdOutFormat(sqlca);
		} catch (Exception e) {
			sqlca = new SqlcaBean(-969, "58033");
			ao = new ArdOutFormat(sqlca);
			log.debug(e);
		} 
		return ao;
	}
	private void execute() throws Exception {
		for(int i = 0; i < in.getNumberOfParameters(); i++){
			stmt.setObject(i + 1, in.getParameter(i));
			log.debug(i);
		}
		rs = stmt.executeQuery();
		ardCon.putResultSet(in.getSectionNumber(), in.getCursorName(), rs);
		rsMetaData = rs.getMetaData();
		createSqldaBean(in.getCcsid());
	}

	void createSqldaBean(int ccsid) throws SQLException {
			int sqlType;
			int scale;
			int precision;
			int columnDisplaySize;
			boolean nullable;
			String columnName;
			short columnCount;
			columnCount = (short )rsMetaData.getColumnCount();
			log.debug("columnCount: " + columnCount);
			sqlda = new SqldaBean(columnCount, 1, in.getCcsid());
			for(int i = 1; i <= columnCount;i++){
				sqlType = rsMetaData.getColumnType(i);
				scale = rsMetaData.getScale(i);
				precision = rsMetaData.getPrecision(i);
				columnDisplaySize = rsMetaData.getColumnDisplaySize(i);
				nullable = (rsMetaData.isNullable(i) == ResultSetMetaData.columnNullable);
				columnName = rsMetaData.getColumnName(i);
				SqlvarBean bean = new  SqlvarBean(sqlType, scale, precision, columnDisplaySize,  
						nullable, columnName, ccsid);
				sqlda.addSqlvar(bean);
			}
	}
}
