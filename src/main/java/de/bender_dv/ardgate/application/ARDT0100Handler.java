/* 
 * File: ARDT0100Handler.java 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.pords2pojo.ARDT0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.ardgate.pords2pojo.SqldaBean;

/**
 * @author Bender
 *	TODO Rudiment, must be implemented
 */
class ARDT0100Handler extends ExtendedSqldaCreator {
	private static final Log log = LogFactory.getLog(ARDT0100Handler.class);
	private ARDT0100Format in;
	private Connection con;
	private PreparedStatement stmt;
	private ResultSetMetaData rsMetaData;
	private String sqlString;
	private SqldaBean sqlda;
	/**
	 * 
	 */
	ARDT0100Handler() {
		super();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormatName);
		in = new ARDT0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		con = ardCon.getConnection();
		sc = ardCon.getSqlstateConverter();
		sqlString = "select * from " + in.getName();
		try {
			prepare();
			sqlca = new SqlcaBean();
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sc);
			e.printStackTrace();
			return new ArdOutFormat(sqlca);
		}
		return new ArdOutFormat(sqlca, sqlda);
	}
	void prepare() throws SQLException{
		int ccsid = ardCon.getCcsid();
		stmt = con.prepareStatement(sqlString);
		try {
			rsMetaData = stmt.getMetaData();
		} catch (SQLException e) {
			rsMetaData = stmt.executeQuery().getMetaData();
		}
		sqlda = createSqldaBean(rsMetaData, in.getCcsid());
		stmt.close();
	}
}
