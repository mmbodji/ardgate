/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.COMM0100Format;
import de.bender_dv.ardgate.pords2pojo.QTNRIFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;

/**
 * @author Bender
 *
 */
class COMM0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(COMM0100Handler.class);
	private COMM0100Format in;
	private QTNRIFormat out;
	private String commitVote = "C";
	/**
	 * 
	 */
	COMM0100Handler() {
		super();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormat);
		in = new COMM0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		if (in.getActionRequired().equals("C")){
			actGroup.commit();
			commitVote = "1";
		}
		else if (in.getActionRequired().equals("R")){
//			if(actGroup != null)
				actGroup.rollback();
			commitVote = "2";
		}
		out = new QTNRIFormat(commitVote);
		SqlcaBean sqlca = new SqlcaBean(0, "00000");
		ArdOutFormat ao = new ArdOutFormat(sqlca, out);		
		return ao;
	}
}
