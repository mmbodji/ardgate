/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.pords2pojo.ARDI0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;

/**
 * @author Bender
 *
 */
class ARDI0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(ARDI0100Handler.class);
	private ARDI0100Format in;
	private Connection con;
	/**
	 * 
	 */
	ARDI0100Handler() {
		super();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormat);
		in = new ARDI0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		if (actGroup == null){
			sqlca = new SqlcaBean (-596, "01002");
			sqlca.setSqlErrMc("Connection does not exist");
			return new ArdOutFormat(sqlca);
		}
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		if (ardCon != null){
			sc = ardCon.getSqlstateConverter();			
		}
		// TODO setTransactionIsolation
		try {
			con = ardCon.getConnection();			
			close();
			sqlca = new SqlcaBean();
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sc);
			sqlca.setSqlErrMc(e.getMessage());
			e.printStackTrace();
		} catch (Exception e){
			sqlca = new SqlcaBean (-596, "01002");
			sqlca.setSqlErrMc("Connection does not exist");
		}
		con = null;
		actGroup.removeAttribute(in.getConnectionKey());
		return new ArdOutFormat(sqlca);
	}

	private void close() throws SQLException {
		con.close();
	}
}
