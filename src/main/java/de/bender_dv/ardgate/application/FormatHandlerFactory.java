/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.jvagate.communication.Session;

/**
 * @author Bender 
 *
 */
class FormatHandlerFactory {
	private static final Log log = LogFactory.getLog(FormatHandlerFactory.class);
	private static FormatHandlerFactory instance;
	static{
		instance = new FormatHandlerFactory();
	}
	/**
	 * 
	 */
	private FormatHandlerFactory() {
		super();
		// TODO Auto-generated constructor stub
	}
	static FormatHandlerFactory getInstance() {
		return instance;
	}
	FormatHandler getFormatHandler(String name, Session session){
		log.debug("Name: " + name);
		FormatHandler result = null;
		if(name.equals("ARFC0100")){
			result = new ARFC0100Handler();			
		}
		else if(name.equals("ARXD0100")){
			result = new ARXD0100Handler();
		}
		else if(name.equals("ARXI0100")){
			result = new ARXI0100Handler();			
		}
		else if(name.equals("ARXB0100")){
			result = new ARXB0100Handler();			
		}		
		else if(name.equals("ARXP0100")){
			result = new ARXP0100Handler();			
		}		
		else if(name.equals("ARCN0100")){
			result = new ARCN0100Handler();
		}
		else if(name.equals("ARDI0100")){
			result = new ARDI0100Handler();			
		}
		else if(name.equals("ARPD0100")){
			result = new ARPD0100Handler();			
		}
		else if(name.equals("ARPS0100")){
			result = new ARPS0100Handler();			
		}
		else if(name.equals("AROC0100")){
			result = new AROC0100Handler();			
		}
		else if(name.equals("ARCC0100")){
			result = new ARCC0100Handler();			
		}		
		else if(name.equals("ARDT0100")){
			result = new ARDT0100Handler();			
		}
		else if(name.equals("COMM0100")){
			result = new COMM0100Handler();			
		}
		else{
			result = new CancelHandler();
		}
		result.setSession(session);
		return result;
	}
}
