/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.Format;
import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.pords2pojo.ARFC0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.OutFetchCursorFormat;
import de.bender_dv.ardgate.pords2pojo.ResultSetHuddle;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;

/**
 * @author Bender
 * 
 */
class ARFC0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(ARFC0100Handler.class);
	private ARFC0100Format in;
	private Connection con;
	private PreparedStatement stmt;
	ResultSet rs;
	private ArdOutFormat ao;
	private OutFetchCursorFormat of;
	protected ResultSetMetaData rsMetaData;
	private boolean isNullable;
	private ResultSetHuddle rh;

	/**
	 * 
	 */
	public ARFC0100Handler() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[],
	 * java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	@Override
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.info("Format: " + inFormat);
		in = new ARFC0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG"
				+ in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		sc = ardCon.getSqlstateConverter();
		con = ardCon.getConnection();
		stmt = ardCon.getStatement(in.getSectionNumber());
		rs = ardCon.getResultSet(in.getSectionNumber());
		try {
			fetch();
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sc);
			of = new OutFetchCursorFormat();
			e.printStackTrace();
		}
		ArdOutFormat out = new ArdOutFormat(sqlca, of);
		return out;
	}

	private void fetch() throws SQLException {
		if (rs.next()) {
			rh = new ResultSetHuddle(in.getCcsid());
			int length;
			int precision;
			int columnCount;
			rsMetaData = rs.getMetaData();
			for (int i = 1; i <= (columnCount = rsMetaData.getColumnCount()); i++) {
				log.debug("columnCount: " + columnCount);
				int ct = rsMetaData.getColumnType(i);
				log.debug("columnType: " + ct);

				isNullable = (rsMetaData.isNullable(i) == ResultSetMetaData.columnNullable);
				if((precision = rsMetaData.getPrecision(i)) < 1){
					precision = rsMetaData.getColumnDisplaySize(i);
				}
				length = precision;
				if (ct == Types.CHAR) {
					String s = rs.getString(i);
					if (isNotNull()) {
						log.debug("CHAR column: " + i + " contents: " + s);
						rh.addChar(s, length);
						log.debug("added to ResultSetHuddle");
					}
				} else if (ct == Types.NCHAR) {
						String s = rs.getString(i);
						if (isNotNull()) {
							log.debug("NCHAR column: " + i + " contents: " + s);
							rh.addGraphic(s, length);
							log.debug("added to ResultSetHuddle");
						}
				} else if (ct == Types.DECIMAL) {
					BigDecimal v = rs.getBigDecimal(i);
					if (isNotNull()) {
						log.debug("DECIMAL column: " + i + " contents: " + v);
						rh.addDecimal(v, length,
								rsMetaData.getScale(i));
					}
				} else if (ct == Types.NUMERIC) {
					BigDecimal v = rs.getBigDecimal(i);
					if (isNotNull()) {
						log.debug("NUMERIC column: " + i + " contents: " + v);
						rh.addNumeric(v, length,
								Math.max(rsMetaData.getScale(i), 0));
					}
				} else if (ct == Types.VARCHAR || ct == Types.LONGVARCHAR) {
					String s = rs.getString(i);
					if (isNotNull()) {
						log.debug("VARCHAR column: " + i + " contents: " + s);
						rh.addVarChar(s);
					}
				} else if (ct == Types.NVARCHAR || ct == Types.LONGNVARCHAR) {
					String s = rs.getString(i);
					if (isNotNull()) {
						log.debug("NVARCHAR column: " + i + " contents: " + s);
						rh.addVarGraphic(s);
					}
				} else if (ct == Types.SMALLINT) {
					short v = rs.getShort(i);
					log.debug("SMALLINT column: " + i + " contents: " + v);
					if (isNotNull()) {
						log.debug("column: " + i + " contents: " + v);
						rh.addShort(v);
					}
				} else if (ct == Types.INTEGER) {
					int v = rs.getInt(i);
					log.debug("INTEGER column: " + i + " contents: " + v);
					if (isNotNull()) {
						log.debug("column: " + i + " contents: " + v);
						rh.addInteger(v);
					}
				} else if (ct == Types.BIGINT) {
					long v = rs.getLong(i);
					if (isNotNull()) {
						log.debug("BIGINT column: " + i + " contents: " + v);
						rh.addBigInt(v);
					}
				} else if (ct == Types.DATE) {
					Date d = rs.getDate(i);
					if (isNotNull()) {
						String v = d.toString();
						log.debug("DATE column: " + i + " contents: " + v);
						rh.addChar(v, v.length());
					}
				} else if (ct == Types.TIME) {
					Time t = rs.getTime(i);
					if (isNotNull()) {
						String v = t.toString();
						log.debug("TIME column: " + i + " contents: " + v);
						rh.addChar(v, v.length());
					}
				} else if (ct == Types.TIMESTAMP) {
					Timestamp t = rs.getTimestamp(i);
					if (isNotNull()) {
						Format formdate = new SimpleDateFormat(
								"yyyy-MM-dd-HH.mm.ss.SSSSS0");
						String v = formdate.format(t);
						log.debug("Timestamp: " + v);
						rh.addChar(v, v.length());
					}
				} else {
					String v = rs.getString(i);
					log.debug("Type divers: " + ct);
					log.debug("DIVERS column: " + i + " contents: " + v);
					rh.addChar(v, length);
				}
			}
			sqlca = new SqlcaBean();
			of = new OutFetchCursorFormat(rh, false);
		} else {
			SQLWarning sw = rs.getWarnings();
			if (sw == null) {
				sqlca = new SqlcaBean(100, "02000");
				rs.close();
			} else {
				sqlca = new SqlcaBean(sw, sc);
			}
			of = new OutFetchCursorFormat();
		}
	}

	private boolean isNotNull() throws SQLException {
		boolean result = true;
		if (isNullable) {
			if (rs.wasNull()) {
				rh.addNullIndicator(-1);
				result = false;
			} else {
				rh.addNullIndicator(0);
			}
		}
		return result;
	}
}
