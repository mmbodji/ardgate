/* File: FormatHandler.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.database.SqlstateConverter;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.jvagate.communication.Context;
import de.bender_dv.jvagate.communication.Session;

/**
 * @author Bender
 *
 */
abstract class FormatHandler {

	protected Session session;
	protected ActivationGroup actGroup;
	protected ArdConnection ardCon;
	protected SqlcaBean sqlca;
	protected SqlstateConverter sc;
	/**
	 * 
	 */
	FormatHandler() {
		super();
	}

	abstract ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName);
	
	public int getCcsid(){
		int ccsid;
		Integer ccsidI = (Integer) session.getAttribute("CCSID");
		if (ccsidI == null){
			ccsid = Context.getInstance().getCcsid();
			session.setAttribute("CCSID", new Integer(ccsid));
		}
		else{
			ccsid = ccsidI.intValue();
		}
		return ccsid;
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
