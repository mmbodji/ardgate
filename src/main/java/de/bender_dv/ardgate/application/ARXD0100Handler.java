/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.application;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Types;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.database.SqlTypeConverter;
import de.bender_dv.ardgate.pords2pojo.ARXB0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.OutExecuteFormat;
import de.bender_dv.ardgate.pords2pojo.ResultSetHuddle;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.ardgate.pords2pojo.SqldaBean;
import de.bender_dv.ardgate.pords2pojo.SqlvarBean;

/**
 * @author Bender
 * TODO not finished rudiment
 */
class ARXD0100Handler extends ExtendedSqldaCreator {
	private static final Log log = LogFactory.getLog(ARXD0100Handler.class);
	private ARXB0100Format in;
	private Connection con;
	private PreparedStatement stmt;
	private ResultSet rs;
	private ArdOutFormat ao;
	private OutExecuteFormat of;
	protected ResultSetMetaData rsMetaData;
	private boolean isNullable;
	private ResultSetHuddle rh;
	private String prepareString;
	protected SqldaBean sqlda;
	/**
	 * 
	 */
	public ARXD0100Handler() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	@Override
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.info("Format: " + inFormat);
		in = new ARXB0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		sc = ardCon.getSqlstateConverter();
		con = ardCon.getConnection();
		try {
			prepareString = in.getPrepareString();
			log.debug("prepareString: " + prepareString);
			if(prepareString.startsWith("CALL ")){
				call();
			}
			else{ // Else was never seen D*B 12.9.2014
//				execute();
//				fetch();				
			}
		} catch (SQLException e) {
			log.debug(e.getSQLState());
			sqlca = new SqlcaBean(e, sc);
			of = new OutExecuteFormat();
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
			sqlca = new SqlcaBean(-969, "58033");
			sqlca.setSqlErrMc("Statement does not exist");
			of = new OutExecuteFormat();
		}
		ArdOutFormat out = new ArdOutFormat(sqlca, of);
		return out;
	}
	private void call() throws SQLException {
		CallableStatement stmt = con.prepareCall(prepareString);
		log.debug("call prepared");
		for(int i = 0; i < in.getNumberOfParameters(); i++){
			log.debug("SqlType: " + in.getSqlType(i));
			stmt.registerOutParameter(i + 1, SqlTypeConverter.toJDBCType(in.getSqlType(i)));
			stmt.setObject(i + 1, in.getParameter(i));
			log.debug("SQLVarBean: " + i + " Wert: " + in.getParameter(i));
		}
		stmt.execute();
		rh = new ResultSetHuddle(in.getCcsid());
		for(int i = 0; i < in.getNumberOfParameters(); i++){
			int ct = SqlTypeConverter.toJDBCType(in.getSqlType(i));
			log.debug("return Value: " + stmt.getObject(i + 1));
			if(ct == Types.CHAR){
				String s;
				if ((s = stmt.getString(i + 1)) == null){
					rh.addNullIndicator(1);
				}
				else{
					rh.addNullIndicator(0);
				}
				rh.addChar(s, in.getSqlLen(i));
			}
			else if(ct == Types.DECIMAL){
				BigDecimal v;
				if( (v = stmt.getBigDecimal(i + 1)) == null){
					rh.addNullIndicator(1);
				}
				else{
					rh.addNullIndicator(0);
				}
				rh.addDecimal(v, in.getPrecision(i), 
						in.getScale(i));
			}
			else if(ct == Types.NUMERIC){
				BigDecimal v;
				if( (v = stmt.getBigDecimal(i + 1)) == null){
					rh.addNullIndicator(1);
				}
				else{
					rh.addNullIndicator(0);
				}
				rh.addNumeric(v,in.getPrecision(i), 
						in.getScale(i));
			}
			else if(ct == Types.VARCHAR
					|| ct == Types.LONGVARCHAR){
				String s;
				if ((s = stmt.getString(i + 1)) == null){
					rh.addNullIndicator(1);
				}
				else{
					rh.addNullIndicator(0);
				}
					rh.addVarChar(s, in.getSqlLen(i) - 2);
			}
			else if(ct == Types.INTEGER){
				int v = stmt.getInt(i + 1);
				rh.addNullIndicator(0);
				rh.addInteger(v);
			}
			else if(ct == Types.BIGINT){
				long v = stmt.getLong(i + 1);
				rh.addNullIndicator(0);
				rh.addBigInt(v);						
			}
			else if(ct == Types.DATE){
				String v = stmt.getDate(i + 1).toString();
				rh.addNullIndicator(0);
				rh.addChar(v, v.length());						
			}
			else if(ct == Types.TIME){
				String v = stmt.getTime(i + 1).toString();
				rh.addNullIndicator(0);
				rh.addChar(v, v.length());	
			}
			else if(ct == Types.TIMESTAMP){
				String v = stmt.getTimestamp(i + 1).toString();
				rh.addNullIndicator(0);
				rh.addChar(v, v.length());						
			}

		}
		// stmt.getParameterMetaData().
		sqlca = new SqlcaBean();
		createSqldaBean(in.getCcsid());
		of = new OutExecuteFormat(sqlda, rh, true);
		stmt.close();
	}

	private void execute() throws Exception{
		stmt = con.prepareStatement(in.getPrepareString());
		for(int i = 0; i < in.getNumberOfParameters(); i++){
			stmt.setObject(i + 1, in.getParameter(i));
			log.debug(i);
		}
		rs = stmt.executeQuery();
	}
	void createSqldaBean(int ccsid) throws SQLException {
		short sqlType;
		int columnDisplaySize;
		boolean nullable;
		String columnName;
		short columnCount;
		columnCount = in.getSqldaBean().getColumnCount();
		log.debug("columnCount: " + columnCount);
		sqlda = new SqldaBean(columnCount, 1, in.getCcsid());
		for(int i = 0; i < columnCount;i++){
			sqlType = in.getSqldaBean().getSqlType(i);
			short sqlLen = in.getSqldaBean().getSqlLen(i);
			columnName = "PARM" + i;
			SqlvarBean bean = new  SqlvarBean(sqlType, sqlLen,  
					          columnName, ccsid);
			sqlda.addSqlvar(bean);
		}
	}
	private void fetch() throws Exception {
		if (rs.next()){
			rh = new ResultSetHuddle(in.getCcsid());
			rsMetaData = rs.getMetaData();
			for(int i = 1; i <= rsMetaData.getColumnCount(); i++){
				int ct = rsMetaData.getColumnType(i);
				isNullable = 
					(rsMetaData.isNullable(i) == ResultSetMetaData.columnNullable);
				if(ct == Types.CHAR){
					String s = rs.getString(i);
					log.debug("column: " + i + " contents: " + s);
					if (isNotNull()){
						rh.addChar(s, rsMetaData.getPrecision(i));
					}
				}
				else if(ct == Types.DECIMAL){
					BigDecimal v = rs.getBigDecimal(i);
					if(isNotNull()){
					rh.addDecimal(v, rsMetaData.getPrecision(i), 
							rsMetaData.getScale(i));
					}
				}
				else if(ct == Types.NUMERIC){
					BigDecimal v = rs.getBigDecimal(i);
					if(isNotNull()){
					rh.addNumeric(v, rsMetaData.getPrecision(i), 
							rsMetaData.getScale(i));
					}
				}
				else if(ct == Types.VARCHAR
						|| ct == Types.LONGVARCHAR){
					String s = rs.getString(i);
					log.debug("column: " + i + " contents: " + s);
					if (isNotNull()){
						rh.addVarChar(s);
					}
				}
				else if(ct == Types.INTEGER){
					int v = rs.getInt(i);
					log.debug("column: " + i + " contents: " + v);
					if(isNotNull()){
						rh.addInteger(v);
					}
				}
				else if(ct == Types.BIGINT){
					long v = rs.getLong(i);
					if(isNotNull()){
						rh.addBigInt(v);						
					}
				}
				else if(ct == Types.DATE){
					String v = rs.getDate(i).toString();
					if(isNotNull()){
						rh.addChar(v, v.length());						
					}					
				}
				else if(ct == Types.TIME){
					String v = rs.getTime(i).toString();
					if(isNotNull()){
						rh.addChar(v, v.length());						
					}					
				}
				else if(ct == Types.TIMESTAMP){
					String v = rs.getTimestamp(i).toString();
					if(isNotNull()){
						rh.addChar(v, v.length());						
					}
				}
				else{
					String v = rs.getString(i);
					log.debug("Type divers: " + ct );
					log.debug("column: " + i + " contents: " + v);
					rh.addChar(v, rsMetaData.getPrecision(i));					
				}
			}
			sqlca = new SqlcaBean();
//			of = new OutExecuteFormat(rh, false);
		}
		else{
			SQLWarning sw = rs.getWarnings();
			if (sw == null){
				sqlca = new SqlcaBean(100, "02000");
			}
			else{
				sqlca = new SqlcaBean(sw, sc);
			}
			of = new OutExecuteFormat();
		}
	}

	private boolean isNotNull() throws SQLException {
		boolean result = true;
		if(isNullable){
			if(rs.wasNull()){
				rh.addNullIndicator(-1);
				result = false;
			}
			else{
				rh.addNullIndicator(0);
			}
		}
		return result;
	}
}
