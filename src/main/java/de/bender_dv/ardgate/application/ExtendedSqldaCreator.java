package de.bender_dv.ardgate.application;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.pords2pojo.SqldaBean;
import de.bender_dv.ardgate.pords2pojo.SqlvarBean;

public abstract class ExtendedSqldaCreator extends FormatHandler {
	private static final Log log = LogFactory.getLog(ExtendedSqldaCreator.class);

	public ExtendedSqldaCreator() {
		super();
	}

	protected SqldaBean createSqldaBean(ResultSetMetaData rm, int ccsid) 
		throws SQLException {
		SqldaBean sqlda;
		int sqlType;
		int scale;
		int precision;
		int columnDisplaySize;
		boolean nullable;
		String columnName;
		short columnCount;
		columnCount = (short) rm.getColumnCount();
		log.debug("columnCount: " + columnCount);
		sqlda = new SqldaBean(columnCount, 2, ccsid);
		for(int i = 1; i <= columnCount;i++){
			sqlType = rm.getColumnType(i);
			scale = rm.getScale(i);
			precision = rm.getPrecision(i);
			columnDisplaySize = rm.getColumnDisplaySize(i);
			nullable = (rm.isNullable(i) == ResultSetMetaData.columnNullable);
			columnName = rm.getColumnName(i);
			SqlvarBean bean = new  SqlvarBean(sqlType, scale, precision, columnDisplaySize, 
					nullable, columnName, ccsid);
			sqlda.addSqlvar(bean);
		}
		for(int i = 1; i <= columnCount;i++){
			columnName = rm.getColumnLabel(i);
			SqlvarBean bean = new  SqlvarBean(columnName, ccsid);
			sqlda.addSqlvar(bean);
		}
		return sqlda;
	}

}