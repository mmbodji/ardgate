/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */

package de.bender_dv.ardgate.application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.ArdConnection;
import de.bender_dv.ardgate.pords2pojo.ARXP0100Format;
import de.bender_dv.ardgate.pords2pojo.ArdOutFormat;
import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.ardgate.pords2pojo.UpdatePerformedFormat;

/**
 * @author Bender
 *
 */
class ARXP0100Handler extends FormatHandler {
	private static final Log log = LogFactory.getLog(ARXP0100Handler.class);
	private ARXP0100Format in;
	private UpdatePerformedFormat of;
	private ArdOutFormat ao;
	private Connection con;
	private String prepareString;
	private PreparedStatement stmt;
	private String updateOcurred = "0";
	/**
	 * 
	 */
	ARXP0100Handler() {
		super();
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.application.FormatHandler#getOutFormat(byte[], java.lang.String, de.bender_dv.jvagate.communication.Session)
	 */
	ArdOutFormat getOutFormat(byte[] inFormat, String inFormatName) {
		log.debug(inFormatName + " " + inFormat);
		in = new ARXP0100Format(inFormat, getCcsid());
		actGroup = (ActivationGroup) session.getAttribute("AG" + in.getActivationGroup());
		ardCon = actGroup.getAttribute(in.getConnectionKey());
		sc = ardCon.getSqlstateConverter();
		con = ardCon.getConnection();
		try {
			execute();
//			sqlca = new SqlcaBean();
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sc);
			sqlca.setSqlErrMc(e.getMessage());
			log.error(e);
		}
		of = new UpdatePerformedFormat(updateOcurred);
		ao = new ArdOutFormat(sqlca, of);
		return ao;
	}
	void execute() throws SQLException{
		int count;
		stmt = ardCon.getStatement(in.getSectionNumber());
		for(int i = 0; i < in.getNumberOfParameters(); i++){
			stmt.setObject(i + 1, in.getParameter(i));
			log.debug(i);
		}
		stmt.execute();
		count = stmt.getUpdateCount();
		log.debug("updateCount: " + count);
		if (count == 0)
			sqlca = new SqlcaBean(100, "02000");
		else
			sqlca = new SqlcaBean();
		return;
	}
}
