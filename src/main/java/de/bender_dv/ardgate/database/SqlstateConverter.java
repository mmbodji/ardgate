/* File: SqlstateConverter.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.database;

import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* The Class SqlstateConverter.
* The *ARDPGM interface of DB2 uses SQLCODE in the returned SQLCA for
* its decisions of the application flow- JDBC fills SQLCODE with
* vendor dependent error numbers, so we have to derive this information from
* SQLSTATE for non DB2 databases. 
* Even though SQLSTATE is ANSI standard, it is used rather different from 
* vendor to vendor.
* The translation from SQLSTATE to SQLCODE depends on the sub protocol of the url.
* 
* 
*/
	public class SqlstateConverter {
	private static final Log log = LogFactory.getLog(SqlstateConverter.class);
	private String subProtocol = "default";
	private ResourceBundle rb;
	private static final int SQL_SYSTEM_ERROR = -901;

	/**
	 * Instantiates a new sqlstate converter.
	 */
	public SqlstateConverter() {
		// use the default Converter
	}
	
	/**
	 * Instantiates a new sqlstate converter.
	 *
	 * @param url the url
	 */
	public SqlstateConverter(String url){
		StringTokenizer st = new StringTokenizer(url, ":");
		try{
			st.nextToken();
			subProtocol = st.nextToken();
			rb = ResourceBundle.getBundle("sqlstate" + subProtocol);
		} catch(Exception e){
			try {
				rb = ResourceBundle.getBundle("sqlstateDefault");
			} catch (Exception e1) {
				log.fatal("failed to load sqlstateDefault.properties");
			}
		}		
	}
	
	/**
	 * Gets the sql code.
	 *
	 * @param e the e
	 * @return the sql code
	 */
	public int getSqlCode(SQLException e){
		int result = SQL_SYSTEM_ERROR;
		if (subProtocol.equals("as400")){
			result = e.getErrorCode();
		}
		else{
			result = getSqlCode(e.getSQLState());
		}
		return result;
	}
	int getSqlCode(String sqlState){
		int result;
		try{
			result = new Integer(rb.getString(sqlState));
		} catch (Exception e){
			try{
				result = new Integer(rb.getString(sqlState.substring(0, 2)));			
			} catch (Exception i){
				result = SQL_SYSTEM_ERROR;
			}
		}
		return result;
	}
}
