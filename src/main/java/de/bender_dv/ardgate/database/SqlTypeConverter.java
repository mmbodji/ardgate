package de.bender_dv.ardgate.database;

import java.sql.Types;

/**
 * @author Bender
 *
 * 	JDBC Type	value	DB2 SQL 	value
 * -------------------------------------
 * 	BIGINT		- 5		INT8		492
 * 	CHAR		1		CHAR		452
 * 	DATE		91		DATE		384	
 * 	DECIMAL		3		PACKED		484
 * 	DOUBLE		8		DOUBLE		480
 * 	FLOAT		6		FLOAT		480
 * 	INTEGER		4		INT4		496
 * 	NUMERIC		2		ZONED		488
 * 	SMALLINT	5		INT2		500
 * 	TIME		92		TIME		388
 * 	TIMESTAMP	93		TIMESTAMP	392
 * 	VARCHAR		12		VARCHAR		448
 * 	
 */
public class SqlTypeConverter {
	public static int toJDBCType(short db2Type){
		if ((db2Type == 452) || (db2Type == 453))
			return Types.CHAR;
		else if((db2Type == 484) || (db2Type == 485))
			return Types.DECIMAL;
		else if((db2Type == 448) || (db2Type == 449))
			return Types.VARCHAR;
		else if((db2Type == 488) || (db2Type == 489))
			return Types.NUMERIC;
		else if((db2Type == 496) || (db2Type == 497))
			return Types.INTEGER;
		else if((db2Type == 384) || (db2Type == 385))
			return Types.DATE;
		else if((db2Type == 388) || (db2Type == 389))
			return Types.TIME;
		else if((db2Type == 392) || (db2Type == 393))
			return Types.TIMESTAMP;
		else if((db2Type == 480) || (db2Type == 481))
			return Types.FLOAT;
		else if((db2Type == 500) || (db2Type == 501))
			return Types.SMALLINT;
		else if((db2Type == 492) || (db2Type == 493))
			return Types.BIGINT;
		else
			return Types.OTHER;
	}
}
