/* File: ArdConnection.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.Hashtable;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.pords2pojo.SqlcaBean;
import de.bender_dv.jvagate.communication.Context;
import de.bender_dv.jvagate.config.JvagateProperties;

/**
 * The Class ArdConnection.
 * ArdConnection represents a single database connect using Ardgate
 * and is identified by the Jobnumber and Activation Group of the AS/400 Job, 
 * and the database name, as configured in the RDBDIR.
 * Ardgate is stateless and ArdConnection is used to store information,
 * that should live longer than one event.
 *
 * @author Bender
 */
/**
 * @author Bender
 * 
 */
public class ArdConnection {
	private static final Log log = LogFactory.getLog(ArdConnection.class);
	private int activationGroup;
	private int ccsid;
	private Connection con = null;
	private Hashtable content;
	private String databaseName;
	private String driver;
	private Properties jdbcProps;
	private String password;
	private JvagateProperties props;
	private Hashtable<Integer, ResultSet> resultSet;
	private Hashtable<Integer, ResultSetMetaData> resultSetMetaData;
	private Hashtable<String, Integer> section;
	private SqlcaBean sqlca;
	private SqlstateConverter sqlstateConverter;
	private Hashtable<Integer, PreparedStatement> statement;
	private String url;
	private String user;
	private String key;
	private boolean autoCommit;
	private short level;

	/**
	 * Instantiates a new ard connection.
	 */
	public ArdConnection() {
		super();
		content = new Hashtable();
		statement = new Hashtable<Integer, PreparedStatement>();
		resultSet = new Hashtable<Integer, ResultSet>();
		section = new Hashtable<String, Integer>();
	}

	/**
	 * Instantiates a new ard connection.
	 * 
	 * @param databaseName
	 *            the database name
	 * @param activationGroup
	 *            the activation group
	 */
	public ArdConnection(String databaseName, int activationGroup) {
		this();
		this.databaseName = databaseName;
		this.activationGroup = activationGroup;
		this.ccsid = Context.getInstance().getCcsid();
	}

	public void commit() {
		try {
			con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/**
	 * Connect to a database
	 * 
	 * @param userId
	 *            the user id
	 * @param passWord
	 *            the pass word
	 * @return true, if successful
	 * @throws ConnectException
	 *             the connect exception
	 */
	public boolean connect(String userId, String passWord)
			throws ConnectException {
		boolean result = false;
		user = userId.trim();
		password = passWord.trim();
		try {
			loadProperties();
			connectDatabase();
			result = true;
			int sqlCode = sqlstateConverter.getSqlCode("08003");
		} catch (SQLException e) {
			int sqlCode = e.getErrorCode();
			// change this rubbish of jt400 driver!
			if (sqlCode == -99999) {
				sqlCode = -30060;
			}
			// sqlca = new SqlcaBean(sqlCode, e.getSQLState());
			// correct Sqlcode
			sqlca = new SqlcaBean(e, sqlstateConverter);
			sqlca.setSqlErrMc("incorrect login information");
			log.error(user + " not connected to " + databaseName + " " + url);
			log.error("SQLCode: " + e.getErrorCode() + " SQLState: "
					+ e.getSQLState());
			log.error(e.getMessage());
			throw new ConnectException("Connect to database failed");
		} catch (Exception e) {
			sqlca = new SqlcaBean(-30061, "08003");
			log.debug("error " + e.getMessage());
			sqlca.setSqlErrMc("configuration Error" + e.getMessage());
			log.error("Driver class not found: " + driver);
			throw new ConnectException("Driver Class not found");
		}
		return result;
	}

	private void connectDatabase() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		if (!password.trim().equals("")) {
			jdbcProps.put("user", user);
			jdbcProps.put("password", password);
		}
		con = DriverManager.getConnection(url, jdbcProps);
		if (autoCommit == false){
			con.setAutoCommit(false);			
		}
		log.debug("Properties" + jdbcProps);
		log.debug(user + " connected to " + databaseName + " " + url);
		sqlca = new SqlcaBean(0, "00000");
	}

	/**
	 * Disconnect from database
	 */
	public void disconnect() {
		try {
			con.close();
			sqlca = new SqlcaBean();
		} catch (SQLException e) {
			sqlca = new SqlcaBean(e, sqlstateConverter);
			e.printStackTrace();
		}
		con = null;
	}

	/**
	 * Gets the Object store under this key
	 * 
	 * @param key
	 *            the key
	 * @return the object returns null if no Object is stored
	 * 
	 */
	public Object get(Object key) {
		return content.get(key);
	}

	/**
	 * Gets the ccsid.
	 * 
	 * @return the ccsid
	 */
	public int getCcsid() {
		return ccsid;
	}

	/**
	 * Gets the database connection.
	 * 
	 * @return the connection
	 */
	public Connection getConnection() {
		// added to get an impression what's happening with warnings
		try {
			for (SQLWarning w = con.getWarnings(); w != null; w = w
					.getNextWarning())
				log.warn("getConnection: " + w);
			con.clearWarnings();
		} catch (SQLException e) {
			log.error("clearWarnings: " + e);
		} catch (Exception e) {
			log.error("clearWarnings: " + e);
		}
		return con;
	}

	/**
	 * Gets the result set.
	 * 
	 * @param sectionNumber
	 *            the section number
	 * @return the result set
	 */
	public ResultSet getResultSet(int sectionNumber) {
		return resultSet.get(sectionNumber);
	}

	/**
	 * Gets the result set.
	 * 
	 * @param cursorName
	 *            the cursor name
	 * @return the result set
	 */
	public ResultSet getResultSet(String cursorName) {
		return resultSet.get(section.get(cursorName));
	}

	/**
	 * Gets the result set meta data.
	 * 
	 * @param sectionNumber
	 *            the section number
	 * @return the result set meta data
	 */
	public ResultSetMetaData getResultSetMetaData(int sectionNumber) {
		return resultSetMetaData.get(sectionNumber);
	}

	/**
	 * Gets the sqlca.
	 * 
	 * @return the sqlca
	 */
	public SqlcaBean getSqlca() {
		return sqlca;
	}

	/**
	 * Gets the sqlstate converter.
	 * 
	 * @return the sqlstate converter
	 */
	public SqlstateConverter getSqlstateConverter() {
		return sqlstateConverter;
	}

	/**
	 * Gets the statement.
	 * 
	 * @param sectionNumber
	 *            the section number
	 * @return the statement
	 */
	public PreparedStatement getStatement(int sectionNumber) {
		return statement.get(sectionNumber);
	}

	private void loadProperties() throws Exception {
		String resource = "url";
		props = (JvagateProperties) Context.getInstance().get(
				"GlobalProperties");
		try {
			url = props.getString("ard.url." + databaseName.trim()).trim();
			resource = "driver";
			driver = props.getString("ard.driver." + databaseName.trim()).trim();
		} catch (Exception e) {
			throw new ConnectException(": missing Property ard." + resource
					+ "." + databaseName.trim() + " in global.properties");
		}
		try{
			// seems to be a little bit strange?!
			props.getString("ard.autocommit." + databaseName.trim());
			setAutoCommit(true);
		} catch (Exception e) {
			setAutoCommit(false);
		}
		
		log.debug("url: " + url);
		jdbcProps = props.getProperties("ard.properties." + databaseName.trim()
				+ ".");
		driver = props.getString("ard.driver." + databaseName.trim()).trim();
		log.debug("driver: " + driver);
		sqlstateConverter = new SqlstateConverter(url);
	}

	/**
	 * Put an Object with a key to get it back later
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void put(Object key, Object value) {
		content.put(key, value);
	}

	/**
	 * Put meta data.
	 * 
	 * @param sectionNumber
	 *            the section number
	 * @param rsmd
	 *            the rsmd
	 */
	public void putMetaData(int sectionNumber, ResultSetMetaData rsmd) {
		resultSetMetaData.put(sectionNumber, rsmd);
		return;
	}

	/**
	 * Put result set.
	 * 
	 * @deprecated
	 * @param sectionNumber
	 *            the section number
	 * @param rs
	 *            the rs
	 */
	public void putResultSet(int sectionNumber, ResultSet rs) {
		resultSet.put(sectionNumber, rs);
		return;
	}

	public void putResultSet(int sectionNumber, String cursorName, ResultSet rs) {
		resultSet.put(sectionNumber, rs);
		section.put(cursorName, sectionNumber);
		return;
	}

	/**
	 * Put statement.
	 * 
	 * @param sectionNumber
	 *            the section number
	 * @param stmt
	 *            the stmt
	 */
	public void putStatement(int sectionNumber, PreparedStatement stmt) {
		statement.put(sectionNumber, stmt);
		return;
	}

	public void rollback() {
		try {
			con.rollback();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/**
	 * Sets the ccsid. should always contain "a best guess" of the ccsid and has
	 * to be set every time, we get an information
	 * 
	 * @param ccsid
	 *            the new ccsid
	 */
	public void setCcsid(int ccsid) {
		this.ccsid = ccsid;
	}

	public void setKey(String connectionKey) {
		key = connectionKey;
	}

	public String getKey() {
		return key;
	}

	public short getLevel() {
		return level;
	}

	public void setLevel(short level) {
		this.level = level;
		if(level == 0){
			autoCommit = true;
		}
	}

	boolean isAutoCommit() {
		return autoCommit;
	}

	void setAutoCommit(boolean autoCommit) {
		if (level > 0){
			this.autoCommit = autoCommit;
		}
		else{
			autoCommit = true;
		}
	}
}
