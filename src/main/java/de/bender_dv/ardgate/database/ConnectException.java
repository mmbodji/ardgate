/* File: ConnectException.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.database;

/**
 * The Class ConnectException.
 */
public class ConnectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new connect exception.
	 */
	public ConnectException() {
	}

	/**
	 * Instantiates a new connect exception.
	 *
	 * @param message the message
	 */
	public ConnectException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new connect exception.
	 *
	 * @param cause the cause
	 */
	public ConnectException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new connect exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public ConnectException(String message, Throwable cause) {
		super(message, cause);
	}

}
