/* File: NotYetImplementedException.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.database;

import java.sql.SQLException;

public class UnreportedSQLException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4288649067135931311L;

	public UnreportedSQLException() {
	}

	public UnreportedSQLException(String message) {
		super(message);
	}

	public UnreportedSQLException(Throwable cause) {
		super(cause);
	}

	public UnreportedSQLException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnreportedSQLException(String cause, String sqlState, int sqlCode) {
		super(cause, sqlState, sqlCode );
	}

}
