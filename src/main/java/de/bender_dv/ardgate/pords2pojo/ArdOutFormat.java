/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * 1	136			sqlca huddle
 * 137	140	int4	ccsid of sqlca huddel
 * 141	144	int4	length of following huddel
 * 145	end	ds		binary huddle, depends on out format
 */
public class ArdOutFormat extends DataStructure{
	private static final Log log = LogFactory.getLog(ArdOutFormat.class);
	private byte[] huddle;
	private SqlcaBean sqlca;
	private int ccsidOfSqlca;
	private int outDataLength;
	private OutFormat out;
	/**
	 * 
	 */
	public ArdOutFormat() {
		super(500);
		// TODO Auto-generated constructor stub
	}
	public ArdOutFormat(SqlcaBean sqlca) {
		super(500);
		huddle = new byte[145];
		setSqlca(sqlca);
		setCcsidOfSqlca(500);
		setOutDataLength(0);
	}
	public ArdOutFormat(SqlcaBean sqlca, OutFormat outFormat) {
		super(500);
		huddle = new byte[outFormat.length() + 145];
		setSqlca(sqlca);
		setCcsidOfSqlca(500);
		setOutDataLength(outFormat.length());
		setOut(outFormat);
	}
	public byte[] getHuddle() {
		return huddle;
	}
	public void setSqlca(SqlcaBean sqlca) {
		this.sqlca = sqlca;
		System.arraycopy(sqlca.getHuddle(), 0, huddle, 0, 136);
	}
	public void setCcsidOfSqlca(int ccsidOfSqlca) {
		this.ccsidOfSqlca = ccsidOfSqlca;
		System.arraycopy(intToAS400Bin4(ccsidOfSqlca) , 0, huddle, 136, 4);
	}
	public void setOutDataLength(int outDataLength) {
		this.outDataLength = outDataLength;
		System.arraycopy(intToAS400Bin4(outDataLength) , 0, huddle, 140, 4);
	}
	public void setOut(OutFormat out) {
		this.out = out;
		System.arraycopy(out.getHuddle(), 0, huddle, 144, out.length());
	}

}
