/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur with following huddle
 * from	to	type
 * 1	4	int4	Offset to ResultSetHuddle
 * 5	5	text	Cursor closed
 * 6				following huddle
 */
public class OutFetchCursorFormat extends GenericOutFormat {
	private static final Log log = LogFactory.getLog(OutFetchCursorFormat.class);
	private byte[] huddle;
	private boolean cursorClosed;
	private int offsetToResultSetHuddle;
	private ResultSetHuddle rsh;
	/**
	 * 
	 */
	public OutFetchCursorFormat() {
		super();
		huddle = new byte[5];
		setOffsetToResultSetHuddle(0);
		setCursorClosed(true);
	}
	public OutFetchCursorFormat(ResultSetHuddle rsh, boolean cursorClosed) {
		super();
		byte[] rsHuddle = rsh.getHuddle();
		huddle = new byte[rsHuddle.length + 5];
		setOffsetToResultSetHuddle(5);
		setCursorClosed(cursorClosed);
		System.arraycopy(rsHuddle, 0, huddle, 5, rsHuddle.length);
	}	
	protected void setCursorClosed(boolean cursorClosed) {
		this.cursorClosed = cursorClosed;
		if (cursorClosed){
			System.arraycopy(stringToAS400Text("1", 1), 0, huddle, 4, 1);
		}
		else{
			System.arraycopy(stringToAS400Text("0", 1), 0, huddle, 4, 1);			
		}
	}
	protected void setOffsetToResultSetHuddle(int offsetToResultSetHuddle) {
		this.offsetToResultSetHuddle = offsetToResultSetHuddle;
		System.arraycopy(intToAS400Bin4(offsetToResultSetHuddle) , 0, huddle, 0, 4);
	}
	public byte[] getHuddle() {
		return huddle;
	}
	public int length() {
		return huddle.length;
	}
}
