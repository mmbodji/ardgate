/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import java.sql.SQLException;
import java.sql.SQLWarning;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.SqlstateConverter;
import de.bender_dv.as400.datastructure.DataStructure;

/**
 * The Class SqlcaBean.
 *
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * 1	8	text	sqlcaId
 * 9	12	int4	sqlcaBc
 * 13	16	int4	sqlCode
 * 17	18	int2	sqlErrMl
 * 19	88	text	sqlErrMc
 * 89	96	text	sqlErrP
 * 97	120	byte[]	sqlErrD
 * 121	131	text	sqlWarn
 * 132	136	text	sqlState
 */
public class SqlcaBean extends DataStructure {
	
	private static final Log log = LogFactory.getLog(SqlcaBean.class);
	private byte[] huddle;
	private String sqlcaId;
	private int sqlcaBc;
	private int sqlCode;
	private short sqlErrMl;
	private String sqlErrMc;
	private String sqlErrP;
	private int sqlErrd1;
	private int sqlErrd2;
	private int sqlErrd3;
	private int sqlErrd4;
	private int sqlErrd5;
	private int sqlErrd6;
	private String sqlWarn;
	private String sqlState;

	/**
	 * Instantiates a new sqlca bean for normal end.
	 */
	public SqlcaBean() {
		this(0, "00000");
	}
	
	/**
	 * Instantiates a new sqlca bean.
	 *
	 * @param w the SQLWarning
	 */
	public SqlcaBean(SQLWarning w){
		this(0, "00000");
	}
	
	/**
	 * Instantiates a new sqlca bean.
	 *
	 * @param sqlCode the sql code
	 * @param sqlState the sql state
	 */
	public SqlcaBean(int sqlCode, String sqlState) {
		super(500);
		log.info("SQLCode: " + sqlCode);
		log.info("SQLState: " + sqlState);
		huddle = new byte[136];
		setSqlcaId("SQLCA   ");	
		setSqlcaBc(136);
		setSqlCode(sqlCode);
		setSqlErrMl((short) 0);
		setSqlErrMc("        ");
		setSqlErrP("ArdGate ");
		setSqlErrd1(0);
		setSqlErrd2(0);
		setSqlErrd3(0);
		setSqlErrd4(0);
		setSqlErrd5(0);
		setSqlErrd6(0);
		setSqlWarn("        ");
		setSqlState(sqlState);
	}
	
	/**
	 * Instantiates a new sqlca bean.
	 *
	 * @param e the SQLException
	 * @param sc the SqlstateConverter
	 */
	public SqlcaBean(SQLException e, SqlstateConverter sc){
		super(500);
		huddle = new byte[136];
		String sqlState = e.getSQLState();
		String message = e.getMessage();
		if (sqlState == null){
			sqlState = "58033";
		}
		sqlCode = sc.getSqlCode(e);
		log.info("SQLCode: " + sqlCode);
		log.info("SQLState: " + sqlState);
		log.info(message);
		setSqlcaId("SQLCA   ");	
		setSqlcaBc(136);
		setSqlCode(sqlCode);
		setSqlErrMl((short) 0);
		setSqlErrMc(message);
		setSqlErrP("        ");
		setSqlErrd1(0);
		setSqlErrd2(0);
		setSqlErrd3(0);
		setSqlErrd4(0);
		setSqlErrd5(0);
		setSqlErrd6(0);
		setSqlWarn("        ");
		setSqlState(sqlState);
	}
	
	/**
	 * Gets the binary huddle.
	 *
	 * @return the huddle
	 */
	public byte[] getHuddle() {
		return huddle;
	}

	/**
	 * Sets the sqlca id.
	 *
	 * @param sqlcaId the new sqlca id
	 */
	public void setSqlcaId(String sqlcaId) {
		this.sqlcaId = sqlcaId;
		System.arraycopy(stringToAS400Text(sqlcaId, 8), 0, huddle, 0, 8);
	}

	/**
	 * Sets the sqlca bc.
	 *
	 * @param sqlcaBc the new sqlca bc
	 */
	public void setSqlcaBc(int sqlcaBc) {
		this.sqlcaBc = sqlcaBc;
		System.arraycopy(intToAS400Bin4(sqlcaBc) , 0, huddle, 8, 4);
	}

	/**
	 * Sets the sql code.
	 *
	 * @param sqlCode the new sql code
	 */
	public void setSqlCode(int sqlCode) {
		this.sqlCode = sqlCode;
		System.arraycopy(intToAS400Bin4(sqlCode) , 0, huddle, 12, 4);
	}

	/**
	 * Sets the sql err ml.
	 *
	 * @param sqlErrMl the new sql err ml
	 */
	public void setSqlErrMl(short sqlErrMl) {
		this.sqlErrMl = sqlErrMl;
		System.arraycopy(shortToAS400Bin2(sqlErrMl) , 0, huddle, 16, 2);
	}

	/**
	 * Sets the sql err mc.
	 *
	 * @param sqlErrMc the new sql err mc
	 */
	public void setSqlErrMc(String sqlErrMc) {
		this.sqlErrMc = sqlErrMc;
		System.arraycopy(stringToAS400Text(sqlErrMc, 70), 0, huddle, 18, 70);
	}

	/**
	 * Sets the sql err p.
	 *
	 * @param sqlErrP the new sql err p
	 */
	public void setSqlErrP(String sqlErrP) {
		this.sqlErrP = sqlErrP;
		System.arraycopy(stringToAS400Text(sqlErrP, 8), 0, huddle, 88, 8);
	}

	/**
	 * Sets the sql errd1.
	 *
	 * @param sqlErrd1 the new sql errd1
	 */
	public void setSqlErrd1(int sqlErrd1) {
		this.sqlErrd1 = sqlErrd1;
		System.arraycopy(intToAS400Bin4(sqlErrd1) , 0, huddle, 96, 4);
	}
	
	/**
	 * Sets the sql errd2.
	 *
	 * @param sqlErrd2 the new sql errd2
	 */
	public void setSqlErrd2(int sqlErrd2) {
		this.sqlErrd2 = sqlErrd2;
		System.arraycopy(intToAS400Bin4(sqlErrd2) , 0, huddle, 100, 4);
	}
	
	/**
	 * Sets the sql errd3.
	 *
	 * @param sqlErrd3 the new sql errd3
	 */
	public void setSqlErrd3(int sqlErrd3) {
		log.debug("sqlErrd3 " + sqlErrd3);
		this.sqlErrd3 = sqlErrd3;
		System.arraycopy(intToAS400Bin4(sqlErrd3) , 0, huddle, 104, 4);
	}
	
	/**
	 * Sets the sql errd4.
	 *
	 * @param sqlErrd4 the new sql errd4
	 */
	public void setSqlErrd4(int sqlErrd4) {
		this.sqlErrd4 = sqlErrd4;
		System.arraycopy(intToAS400Bin4(sqlErrd4) , 0, huddle, 108, 4);
	}
	
	/**
	 * Sets the sql errd5.
	 *
	 * @param sqlErrd5 the new sql errd5
	 */
	public void setSqlErrd5(int sqlErrd5) {
		this.sqlErrd5 = sqlErrd5;
		System.arraycopy(intToAS400Bin4(sqlErrd5) , 0, huddle, 112, 4);
	}
	
	/**
	 * Sets the sql errd6.
	 *
	 * @param sqlErrd6 the new sql errd6
	 */
	public void setSqlErrd6(int sqlErrd6) {
		this.sqlErrd6 = sqlErrd6;
		System.arraycopy(intToAS400Bin4(sqlErrd6) , 0, huddle, 116, 4);
	}
	
	/**
	 * Sets the sql warn.
	 *
	 * @param sqlWarn the new sql warn
	 */
	public void setSqlWarn(String sqlWarn) {
		this.sqlWarn = sqlWarn;
		System.arraycopy(stringToAS400Text(sqlWarn, 11), 0, huddle, 120, 11);
	}
	
	/**
	 * Sets the sql state.
	 *
	 * @param sqlState the new sql state
	 */
	public void setSqlState(String sqlState) {
		this.sqlState = sqlState;
		System.arraycopy(stringToAS400Text(sqlState, 5), 0, huddle, 131, 5);
	}
}
