/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * exit Info
 * 1	4	int4	ActivationGroup
 * 5	14	text	ActivationGroupA
 * 15	80	text	dummy
 * status Information
 * 
 * */
public class COMM0100Format extends DataStructure {
	private static final Log log = LogFactory.getLog(COMM0100Format.class);
	private int activationGroup;
	private byte[] work;
	private String activationGroupA;
	private int statusLength;
	private String actionRequired;
	private String calledIpl;
	private String errorStatus;
	private String endStatus;
	private String commitRollbackQualifier;
	private String commitScope;
	private String defStatus;
	private int cycleId;
	private String journal;
	private String journalLib;
	private String curLuwId;
	private String cycleIdLong;
	/**
	 * 
	 */
	public COMM0100Format() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param huddle
	 */
	public COMM0100Format(byte[] huddle, int ccsid) {
		super(ccsid);

		setHuddle(huddle);
	}

	public void setHuddle(byte[] huddle) {
		activationGroup = aS400Bin4ToInt(huddle, 0);
		log.debug("Activation Group: " + activationGroup);
		work = new byte[10];
		System.arraycopy(huddle, 4, work, 0, 10);
		activationGroupA = aS400TextToString(work);  
		log.debug("Activation Group: " + activationGroupA);	

		statusLength = aS400Bin4ToInt(huddle, 80);
//		log.debug("Status length: " + statusLength);
		work = new byte[1];
		work[0]= huddle[84];
		actionRequired = aS400TextToString(work);
		log.debug("Action required: " + actionRequired);
		work[0]= huddle[85];
		calledIpl = aS400TextToString(work);
		work[0]= huddle[90];
		errorStatus = aS400TextToString(work);
		work[0]= huddle[91];
		endStatus = aS400TextToString(work);
		work[0]= huddle[93];
		commitRollbackQualifier = aS400TextToString(work);
		work[0]= huddle[94];
		commitScope = aS400TextToString(work);
		log.debug("Commit scope: " + commitScope);
		work[0]= huddle[184];
		defStatus = aS400TextToString(work);
		cycleId = aS400Bin4ToInt(huddle, 120);
		work = new byte[10];
		System.arraycopy(huddle, 124, work, 0, 10);
		journal = aS400TextToString(work);
//		log.debug("Journal: " + journal);
		System.arraycopy(huddle, 134, work, 0, 10);
		journalLib = aS400TextToString(work);
		log.debug("JournalLib: " + journalLib);
		work = new byte[39];
		System.arraycopy(huddle, 144, work, 0, 39);
		curLuwId = aS400TextToString(work);
		work = new byte[20];
		System.arraycopy(huddle, 192, work, 0, 20);
		cycleIdLong = aS400TextToString(work);
	}

	public int getActivationGroup() {
		return activationGroup;
	}

	public String getActivationGroupA() {
		return activationGroupA;
	}

	public int getStatusLength() {
		return statusLength;
	}

	public String getActionRequired() {
		return actionRequired;
	}

	public String getCalledIpl() {
		return calledIpl;
	}

	public String getErrorStatus() {
		return errorStatus;
	}

	public String getEndStatus() {
		return endStatus;
	}

	public String getCommitRollbackQualifier() {
		return commitRollbackQualifier;
	}

	public String getCommitScope() {
		return commitScope;
	}

	public String getDefStatus() {
		return defStatus;
	}

	public int getCycleId() {
		return cycleId;
	}

	public String getJournal() {
		return journal;
	}

	public String getJournalLib() {
		return journalLib;
	}

	public String getCurLuwId() {
		return curLuwId;
	}

	public String getCycleIdLong() {
		return cycleIdLong;
	}

}
