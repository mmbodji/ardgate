/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * binary huddle, representing DataStructur with variable length
 * from	to	type
 * 1	4	int4	interfaceLevel contains 1
 * 5	12	char 8	informatName
 * 13	16	int4	length of format
 * 17	end	ds		binary huddle, depends on informatName	
 */
public class ArdInFormat extends DataStructure {
	private static final Log log = LogFactory.getLog(ArdInFormat.class);
	private byte[] varHuddle;
	private int interfaceLevel;
	private String inFormatName;
	private int lengthOfInFormat;
	private byte[] inFormat;
	public ArdInFormat() {
		super(500);
		// TODO Auto-generated constructor stub
	}
	public ArdInFormat(byte[] varHuddle) {
		super(500);
		this.varHuddle = varHuddle;
		this.interfaceLevel = aS400Bin4ToInt(varHuddle, 0);
		log.debug("interfaceLevel: " + interfaceLevel);
		byte[] work = new byte[8];
		System.arraycopy(varHuddle, 4, work, 0, 8);
		this.inFormatName = aS400TextToString(work);  
		log.debug("inFormatName: " + inFormatName);
		this.lengthOfInFormat = aS400Bin4ToInt(varHuddle, 12);
		log.debug("lengthOfInFormat: " + lengthOfInFormat);
		inFormat = new byte[this.lengthOfInFormat];
		System.arraycopy(varHuddle, 16, inFormat, 0, this.lengthOfInFormat);
	}
	public int getInterfaceLevel() {
		return interfaceLevel;
	}
	public String getInFormatName() {
		return inFormatName;
	}
	public int getLengthOfInFormat() {
		return lengthOfInFormat;
	}
	public byte[] getInFormat() {
		return inFormat;
	}
}
