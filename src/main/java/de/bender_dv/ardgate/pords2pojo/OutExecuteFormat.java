/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * TODO not yet finished rudiment
 * @author Bender
 * binary huddle, representing DataStructur with following huddle
 * from	to	type
 * 1	1	text	Update performed
 * 2	4	text	reserved
 * 5	8	int4	Offset to SQLDA
 * 9	12	int4	Offset to ResultSetHuddle
 * 					following huddle
 */
public class OutExecuteFormat extends GenericOutFormat {
	private static final Log log = LogFactory.getLog(OutExecuteFormat.class);
	private byte[] huddle;
	private String updatePerformed;
	private int offsetToSqldaHuddle;
	private int offsetToResultSetHuddle;
	private ResultSetHuddle rsh;
	/**
	 * 
	 */
	public OutExecuteFormat() {
		super();
		huddle = new byte[16];
		setOffsetToResultSetHuddle(0);
	}
	public OutExecuteFormat(SqldaBean sqlda, ResultSetHuddle rsh, boolean cursorClosed) {
		super();
		byte[] sqldaHuddle = sqlda.getHuddle();
		byte[] rsHuddle = rsh.getHuddle();
		int offset = sqldaHuddle.length / 16;
		offset = offset *16 + 32;
		huddle = new byte[rsHuddle.length + offset];
		setUpdatePerformed(0);
		setReserved();
		setOffsetToSqldaHuddle(16);
		setOffsetToResultSetHuddle(offset);
		System.arraycopy(sqldaHuddle, 0, huddle, 16, sqldaHuddle.length);
		System.arraycopy(rsHuddle, 0, huddle, offset, rsHuddle.length);	
	}
	protected void setOffsetToResultSetHuddle(int offsetToResultSetHuddle) {
		this.offsetToResultSetHuddle = offsetToResultSetHuddle;
		System.arraycopy(intToAS400Bin4(offsetToResultSetHuddle) , 0, huddle, 8, 4);
	}
	public byte[] getHuddle() {
		return huddle;
	}
	public int length() {
		return huddle.length;
	}
	protected void setOffsetToSqldaHuddle(int offsetToSqldaHuddle) {
		this.offsetToSqldaHuddle = offsetToSqldaHuddle;
		System.arraycopy(intToAS400Bin4(offsetToSqldaHuddle), 0, huddle, 4, 4);
	}
	void setUpdatePerformed(int updatePerformed){
		if(updatePerformed == 0){
			System.arraycopy(stringToAS400Text("0", 1), 0, huddle, 0, 1);
		}
		else{
			System.arraycopy(stringToAS400Text("1", 1), 0, huddle, 0, 1);			
		}
			
	}	void setReserved(){
		String reserved = "   ";
		System.arraycopy(stringToAS400Text(reserved, 3), 0, huddle, 1, 3);
	}
}
