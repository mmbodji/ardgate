/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * binary huddle, representing DataStructur with variable length
 * from	to	type
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * 27	end	ds		binary huddle, described in sub classes	
 */
public abstract class GenericArFormat extends DataStructure {
	private static final Log log = LogFactory.getLog(GenericArFormat.class);
	private int outBufferSize;
	private int activationGroup;
	private String databaseName;
	private byte[] huddle;
	public GenericArFormat() {
		super(500);
	}
	public GenericArFormat(byte[] huddle, int ccsid) {
		super(ccsid);
		this.huddle = huddle;
		outBufferSize = aS400Bin4ToInt(huddle, 0);
		log.debug("outBufferSize: " + outBufferSize);
		activationGroup = aS400Bin4ToInt(huddle, 4);
		byte[] work = new byte[18];
		System.arraycopy(huddle, 8, work, 0, 18);
		databaseName = aS400TextToString(work);  
		log.info("***** " + databaseName + ":" + activationGroup + " *****");
	}
	public int getOutBufferSize() {
		return outBufferSize;
	}
	public int getActivationGroup() {
		return activationGroup;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public String getConnectionKey(){
		return databaseName.trim() + Integer.toString(activationGroup).trim();
	}
}
