/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * 1	8	text	sqldaId	contains SQLDA
 * 9	12	int4	sqldaBc := 2 * sqlD * sqlvar.length + 16
 * 							(C declaration long?)
 * 13	14	int2	sqlN	
 * 15	16	int2	sqlD	number of columns described
 * follows array of binary huddle Sqlvar[sqlD]
 * followed by huddle[sqlD] containing x'00' 
// TODO	
 * 
 **/
public class SqldaBean extends DataStructure implements OutFormat{
	private static final Log log = LogFactory.getLog(SqldaBean.class);
	private byte[] huddle;
	private String sqldaId;
	private int sqldaBc;
	private short sqlN;
	private short sqlD;
	private int lengthOfHuddle;
	private int startOfHuddle;
	private Vector<SqlvarBean> v;
	public static final int SIMPLE = 1;
	public static final int DOUBLE = 2;
	private byte[] shudder;

	SqldaBean() {
		super(500);
		// does not work stand alone
	}
	public SqldaBean(short numberOfColumns, int beansPerColumn, int ccsid) {
		super(ccsid);
		startOfHuddle = 16;
		lengthOfHuddle = beansPerColumn * numberOfColumns * 80 + 16;
		this.huddle = new byte[lengthOfHuddle];
		setSqldaId("SQLDA   ");
		setSqldaBc(lengthOfHuddle);
		setSqlD(numberOfColumns);
		setSqlN(numberOfColumns);
	}
	public SqldaBean(byte[] huddle, byte[] shudder, int ccsid) {
		super(ccsid);
		byte[] work;
		int start = 0;
		int length = 0;
		this.huddle = huddle;
		this.shudder = shudder;
		sqldaBc = aS400Bin4ToInt(huddle, 8);
		log.debug("SqldaBc: " + sqldaBc);
		sqlN = aS400Bin2ToShort(huddle, 12);
		log.debug("SqlN: " + sqlN);
		sqlD = aS400Bin2ToShort(huddle, 14);
		log.debug("SqlD: " + sqlD);
		v = new Vector<SqlvarBean>(sqlD);
		for(int i = 0; i < sqlD; i++){
			byte[] h = new byte[80];
			System.arraycopy(huddle, 16 + i *80, h, 0, 80);
			SqlvarBean s = new SqlvarBean(h, ccsid);
			if ((length = s.getLengthOfShudder()) == -1){
				// seems to be varchar
				work = new byte[2];
				System.arraycopy(shudder, start, work, 0, 2);
				length = s.getLengthOfShudder(work);
			}
			work = new byte[length];
			System.arraycopy(shudder, start, work, 0, length);
			s.setShudder(work);
			start = start + length;
			v.add(s);
			if(v.get(i).isNullable()){
				work = new byte[2];
				System.arraycopy(shudder, start, work, 0, 2);
				v.get(i).setIsNull(work);
				start = start + 2;
			}
		}
	}
	public byte[] getHuddle() {
		return huddle;
	}
	public void addSqlvar(SqlvarBean sqlvar){
		log.debug("COLUMN: " + sqlvar.getName()	+ " SQLTYPE: " + sqlvar.getSqlType());
		System.arraycopy(sqlvar.getHuddle(), 0, huddle, startOfHuddle, sqlvar.getHuddle().length);
		startOfHuddle = startOfHuddle + sqlvar.getHuddle().length;
	}

	public void setSqldaId(String sqldaId) {
		this.sqldaId = sqldaId;
		System.arraycopy(stringToAS400Text(sqldaId, 8), 0, huddle, 0, 8);
	}

	public void setSqldaBc(int sqldaBc) {
		this.sqldaBc = sqldaBc;
		System.arraycopy(intToAS400Bin4(sqldaBc) , 0, huddle, 8, 4);
	}

	public void setSqlN(short sqlN) {
		this.sqlN = sqlN;
		System.arraycopy(shortToAS400Bin2(sqlN) , 0, huddle, 12, 2);
	}

	public void setSqlD(short sqlD) {
		this.sqlD = sqlD;
		System.arraycopy(shortToAS400Bin2(sqlD) , 0, huddle, 14, 2);
	}
	public int length() {
		return huddle.length;
	}
	public int getSqlD() {
		return sqlD;
	}
	public Object getSqlVar(int i) {
		SqlvarBean sv = v.get(i);
		return sv.getObject();
	}
	public short getSqlType(int i) {
		SqlvarBean sv = v.get(i);
		return sv.getSqlType();
	}
	public short getSqlLen(int i) {
		SqlvarBean sv = v.get(i);
		return sv.getSqlLen();
	}
	public int getPrecision(int i) {
		SqlvarBean sv = v.get(i);
		return sv.getPrecision();
	}
	public int getScale(int i) {
		SqlvarBean sv = v.get(i);
		return sv.getScale();
	}
	public short getColumnCount() {
		// columnCount is just an alias for sqlD
		return sqlD;
	}
}
