package de.bender_dv.ardgate.pords2pojo;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender binary huddle, representing ResultSetHuddle used to implement
 *         dynamic byte[] to collect data of ResultSet in AS/400 huddle Format
 */
public class ResultSetHuddle extends DataStructure {
	private static final Log log = LogFactory.getLog(ResultSetHuddle.class);
	final int MAX_PRECISION = 31;
	private ByteArrayOutputStream huddle;
	// TODO get ccsid from properties?
	private int unicodeCcsid = 1200;

	public ResultSetHuddle() {
		super(500);
		huddle = new ByteArrayOutputStream();
	}

	public ResultSetHuddle(int ccsid) {
		super(ccsid);
		huddle = new ByteArrayOutputStream();
	}

	public byte[] getHuddle() {
		byte[] bytAry = huddle.toByteArray();
		return bytAry;
	}

	public void addNullIndicator(int b) {
		huddle.write(b);
		return;
	}

	public void addChar(String data, int length) {
		log.debug("data: " + data + "length: " + length);
		huddle.write(stringToAS400Text(data, length), 0, length);
		log.debug("added to huddle");
		return;
	}

	public void addGraphic(String data, int length) {
		log.debug("data: " + data + "length: " + length);
		huddle.write(stringToAS400Text(data, length * 2, unicodeCcsid), 0,
				length * 2);
		log.debug("added to huddle");
		return;
	}

	public void addVarGraphic(String data, int length) {
		log.debug("data: " + data + "length: " + length);
		huddle.write(stringToAS400VarText(data, length * 2, unicodeCcsid), 0,
				length * 2 + 2);
		log.debug("added to huddle");
		return;
	}

	public void addVarGraphic(String data) {
		int length = data.length();
		log.debug("data: " + data + "length: " + length);
		huddle.write(stringToAS400VarText(data, length * 2, unicodeCcsid), 0,
				length * 2 + 2);
		log.debug("added to huddle");
		return;
	}

	public void addVarChar(String data) {
		// caution, length must be incremented by 2
		huddle.write(stringToAS400VarText(data), 0, data.length() + 2);
		return;
	}

	public void addShort(short value) {
		huddle.write(shortToAS400Bin2(value), 0, 2);
		return;
	}

	public void addInteger(int value) {
		huddle.write(intToAS400Bin4(value), 0, 4);
		return;
	}

	public void addBigInt(long value) {
		huddle.write(longToAS400Bin8(value), 0, 8);
		return;
	}

	public void addNumeric(BigDecimal value, int precision, int scale) {
		byte[] shudder = bigDecimalToAS400ZonedDecimal(value,
				Math.min(precision, MAX_PRECISION), scale);
		huddle.write(shudder, 0, shudder.length);
		return;
	}

	public void addDecimal(BigDecimal value, int precision, int scale) {
		byte[] shudder = bigDecimalToAS400PackedDecimal(value, precision, scale);
		huddle.write(shudder, 0, shudder.length);
		return;
	}

	public void addVarChar(String data, int length) {
		byte[] shudder = stringToAS400VarText(data, length);
		huddle.write(shudder, 0, shudder.length);		
	}

}
