/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * GenericArxFormat
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 * end of inherited fields
 * 113	116	int4	Offset to following huddle
 * 117	120	int4	length of following huddle
 * 			text	SQL statement
 * */
public class ARXI0100Format extends GenericArxFormat {
	private static final Log log = LogFactory.getLog(ARXI0100Format.class);
	private int offsetToSqlStatement;
	private int lengthOfSqlStatement;
	private String sqlStatement;
	/**
	 * 
	 */
	public ARXI0100Format() {
	}

	/**
	 * @param huddle
	 */
	public ARXI0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		byte[] work;

		offsetToSqlStatement = aS400Bin4ToInt(huddle, 112);
		log.debug("offsetToSqlStatement: " + offsetToSqlStatement);

		lengthOfSqlStatement = aS400Bin4ToInt(huddle, 116);
		log.debug("lengthOfSqlStatement: " + lengthOfSqlStatement);

		work = new byte[lengthOfSqlStatement];
		System.arraycopy(huddle, offsetToSqlStatement, work, 0, lengthOfSqlStatement);
		sqlStatement = aS400TextToString(work);  
		log.debug("sqlStatement: " + sqlStatement);	

	}

	public int getOffsetToSqlStatement() {
		return offsetToSqlStatement;
	}

	public int getLengthOfSqlStatement() {
		return lengthOfSqlStatement;
	}

	public String getSqlStatement() {
		return sqlStatement;
	}

}
