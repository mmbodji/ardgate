/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * end of inherited fields
 * 27	36	text	Device name
 * 37	44	text	Mode name
 * 45	52	text	Remote location
 * 53	60	text	Local location
 * 61	68	text	Remote network identifier
 * 69	76	text	TPN name
 * 77	86	text	UserId
 * 77	96	text	Password
 * 97	104	text	Product identifier
 * 
 * */
public class ARCN0100Format extends GenericArFormat {
	private static final Log log = LogFactory.getLog(ARCN0100Format.class);
	private String deviceName;
	private String modeName;
	private String remoteLoc;
	private String localLoc;
	private String remoteNetId;
	private String tpnName;
	private String userId;
	private String password;
	private String productId;
	private short level;

	/**
	 * 
	 */
	public ARCN0100Format() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param huddle
	 */
	public ARCN0100Format(byte[] huddle, int ccsid){
		super(huddle, ccsid);

		byte[] work = new byte[10];
		System.arraycopy(huddle, 26, work, 0, 10);
		deviceName = aS400TextToString(work);  
		log.debug("deviceName: " + deviceName);	

		work = new byte[8];
		System.arraycopy(huddle, 36, work, 0, 8);
		modeName = aS400TextToString(work);  
		log.debug("modeName: " + modeName);	

		System.arraycopy(huddle, 44, work, 0, 8);
		remoteLoc = aS400TextToString(work);  
		log.debug("remoteLoc: " + remoteLoc);
		
		System.arraycopy(huddle, 52, work, 0, 8);
		localLoc = aS400TextToString(work);  
		log.debug("localLoc: " + localLoc);	

		System.arraycopy(huddle, 60, work, 0, 8);
		remoteNetId = aS400TextToString(work);  
		log.debug("remoteNetId: " + remoteNetId);
		
		System.arraycopy(huddle, 68, work, 0, 8);
		tpnName = aS400TextToString(work);  
		log.debug("tpnName: " + tpnName);
		
		System.arraycopy(huddle, 96, work, 0, 8);
		productId = aS400TextToString(work);  
		log.debug("productId: " + productId);	

		work = new byte[10];
		System.arraycopy(huddle, 76, work, 0, 10);
		userId = aS400TextToString(work);  
		log.debug("userId: " + userId);	

		System.arraycopy(huddle, 86, work, 0, 10);
		password = aS400TextToString(work);  
		log.debug("password: " + password);	
		
		if(huddle.length == 106){
			level = aS400Bin2ToShort(huddle, 104);
		}
		else{
			level = 0;
		}
		log.debug("Level: " + level);
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getModeName() {
		return modeName;
	}

	public String getRemoteLoc() {
		return remoteLoc;
	}

	public String getLocalLoc() {
		return localLoc;
	}

	public String getRemoteNetId() {
		return remoteNetId;
	}

	public String getTpnName() {
		return tpnName;
	}

	public String getUserId() {
		return userId;
	}

	public String getPassword() {
		return password;
	}

	public String getProductId() {
		return productId;
	}

	public short getLevel() {
		return level;
	}
}
