/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur with following huddle
 * from	to	type
 * 1	1	text	Block data
 * 2	2	text	Cursor held
 * 3	4	text	reserved
 * 5	8	int4	offset to SqldaHuddle
 * 9		huddle	Sqlda
 */
public class OutOpenCursorFormat extends GenericOutFormat {
	private static final Log log = LogFactory.getLog(OutOpenCursorFormat.class);
	private byte[] huddle;
	private boolean blockData;
	private boolean cursorHeld;
	private int offsetToSqldaHuddle;
	private SqldaBean sqlda;
	/**
	 * 
	 */
	public OutOpenCursorFormat() {
		super();
		huddle = new byte[8];
		setBlockData(false);
		setCursorHeld(true);
		setOffsetToSqldaHuddle(0);
	}
	public OutOpenCursorFormat(SqldaBean sqlda) {
		super();
		huddle = new byte[sqlda.length() + 16];
		setBlockData(false);
		setCursorHeld(true);
		int aetsch = 16;
		setOffsetToSqldaHuddle(aetsch);
		setSqlda(sqlda);
	}	
	protected void setBlockData(boolean blockData) {
		this.blockData = blockData;
		if (blockData){
			System.arraycopy(stringToAS400Text("1", 1), 0, huddle, 0, 1);
		}
		else{
			System.arraycopy(stringToAS400Text("0", 1), 0, huddle, 0, 1);			
		}
	}
	protected void setCursorHeld(boolean cursorHeld) {
		this.cursorHeld = cursorHeld;
		if (cursorHeld){
			System.arraycopy(stringToAS400Text("1", 3), 0, huddle, 1, 1);
		}
		else{
			System.arraycopy(stringToAS400Text("0", 1), 0, huddle, 1, 1);			
		}
	}
	protected void setOffsetToSqldaHuddle(int offsetToSqldaHuddle) {
		this.offsetToSqldaHuddle = offsetToSqldaHuddle;
		System.arraycopy(intToAS400Bin4(offsetToSqldaHuddle) , 0, huddle, 4, 4);
	}
	protected void setSqlda(SqldaBean sqlda) {
		this.sqlda = sqlda;
		System.arraycopy(sqlda.getHuddle() , 0, huddle, 
				16, sqlda.length());
	}
	public byte[] getHuddle() {
		return huddle;
	}
	public int length() {
		return huddle.length;
	}
}
