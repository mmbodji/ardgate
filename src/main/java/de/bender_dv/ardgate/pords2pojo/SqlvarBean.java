/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import java.sql.Types;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender 
 * binary huddle, representing binary huddle 
 * from to 	type 
 * 	1	2	int2 	sqlType 
 * 	3	4 	int2 	sqlLen 
 * 	5 	16 	text 	sqlRes 
 * 	17 	32 			sqlData huddle, shudder - Pointer or not Pointer 
 * 	33 	48 			sqlInd huddle, shudder - Pointer or not Pointer 
 * 	49 	50 	int2 	nameLength 
 * 	51 	80 	text	name huddle, shudder - or 2 Bytes CCSID
 */
public class SqlvarBean extends DataStructure {
	private static final Log log = LogFactory.getLog(SqlvarBean.class);
	static final int MAX_PRECISION = 31;
	static final int MAX_SCALE = 31;
	private byte[] huddle;
	private short sqlType;
	private short sqlLen;
	private short hex00;
	private short ccsid;
	private short nameLength;
	private String name;
	private boolean nullable = false;
	private boolean isNull = false;
	private byte[] work;
	private byte[] shudder;
	private int mudP;
	private int mudIndP;

	SqlvarBean() {
		super(500);
	}

	public SqlvarBean(byte[] huddle, int ccsid) {
		super(ccsid);
		this.huddle = huddle;
		sqlType = aS400Bin2ToShort(huddle, 0);
		log.debug("SqlType: " + sqlType);
		if (sqlType == 448
			|| sqlType == 456){
			sqlLen = -1;
		}
		else {
			sqlLen = aS400Bin2ToShort(huddle, 2);			
		}
		log.debug("SqlLen: " + sqlLen);
		this.ccsid = aS400Bin2ToShort(huddle, 52);
		log.debug("CCSID: " + ccsid);
//		setConvCcsid(ccsid);
		mudP = aS400Bin4ToInt(huddle, 28);
		log.debug("Pointer to mud: " + mudP);
		mudIndP = aS400Bin4ToInt(huddle, 44);
		log.debug("Pointer to mudInd: " + mudIndP);
	}

	/**
	 * @param sqlType
	 *            as in java.sql.Types
	 * @param scale
	 *            as used in ResultSetMetaData
	 * @param precision
	 *            as used in ResultSetMetaData
	 * @param columnName
	 * @param ccsid
	 *            use best guess of Connection
	 */
	public SqlvarBean(int sqlType, int scale, int precision, int columnDisplaySize, boolean nullable,
			String columnName, int ccsid) {
		super(ccsid);
		huddle = new byte[80];
		setNameLength((short) 30);
		setName(columnName);
		setCcsid((short) ccsid);
		int corr = 0;
		if (nullable) {
			corr = 1;
		}
		if (precision <= 0){
			log.warn("precision " + precision + " not valid, trying columnDisplaySize " + columnDisplaySize);
			precision = columnDisplaySize;
		}
		if (scale < 0){
			log.warn("scale " + scale + " not valid, trying 0 ");
			scale = 0;
		}
		log.debug("precision: " + precision + " scale: " + scale);
		if (sqlType == Types.CHAR) {
			this.sqlType = (short) (452 + corr);
			sqlLen = (short) precision;
		} else if (sqlType == Types.DECIMAL) {
			this.sqlType = (short) (484 + corr);
			sqlLen = (short) (256 * precision + scale);
		} else if (sqlType == Types.NUMERIC) {
			this.sqlType = (short) (488 + corr);
			sqlLen = (short) (256 * Math.min(precision, MAX_PRECISION) + scale);
		} else if (sqlType == Types.VARCHAR) {
			this.sqlType = (short) (448 + corr);
			sqlLen = (short) precision;
		} else if (sqlType ==  Types.LONGVARCHAR) {
			this.sqlType = (short) (456 + corr);
			sqlLen = (short) precision;
		} else if (sqlType == Types.NCHAR) {
			this.sqlType = (short) (468 + corr);
			sqlLen = (short) precision;
			setCcsid((short) 1200);
		} else if (sqlType == Types.NVARCHAR) {
			this.sqlType = (short) (464 + corr);
			sqlLen = (short) precision;
			setCcsid((short) 1200);
		} else if (sqlType ==  Types.LONGNVARCHAR) {
			this.sqlType = (short) (472 + corr);
			sqlLen = (short) precision;
		} else if (sqlType == Types.SMALLINT) {
			this.sqlType = (short) (500 + corr);
			sqlLen = 2;
		} else if (sqlType == Types.INTEGER) {
			this.sqlType = (short) (496 + corr);
			sqlLen = 4;
		} else if (sqlType == Types.BIGINT) {
			this.sqlType = (short) (492 + corr);
			sqlLen = 8;
		} else if (sqlType == Types.DATE) {
			this.sqlType = (short) (384 + corr);
			sqlLen = 10;
		} else if (sqlType == Types.TIME) {
			this.sqlType = (short) (388 + corr);
			sqlLen = 8;
		} else if (sqlType == Types.TIMESTAMP) {
			this.sqlType = (short) (392 + corr);
			sqlLen = 26;
		} else {
			log.warn("sqlType " + sqlType + " unknown, trying char!");
			this.sqlType = (short) (452 + corr);
			sqlLen = (short) precision;
		}
		setSqlType(this.sqlType);
		setSqlLen(sqlLen);
	}

	public SqlvarBean(String columnLabel, int ccsid) {
		super(ccsid);
		huddle = new byte[80];
		setNameLength((short) 30);
		setName(columnLabel);
		setCcsid((short) ccsid);
	}

	public SqlvarBean(short sqlType, short sqlLen, String columnName, int ccsid) {
		super(ccsid);
		log.debug("SqlType: " + sqlType + " SqlLen: " + sqlLen + " ColumnName: " + columnName + " Ccsid: " + ccsid);
		huddle = new byte[80];
		setNameLength((short) 30);
		setName(columnName);
		setCcsid((short) ccsid);
		setSqlType(sqlType);
		setSqlLen(sqlLen);
	}

	public byte[] getHuddle() {
		return huddle;
	}

	public void setSqlType(short sqlType) {
		this.sqlType = sqlType;
		System.arraycopy(shortToAS400Bin2(sqlType), 0, huddle, 0, 2);
	}

	public void setSqlLen(short sqlLen) {
		this.sqlLen = sqlLen;
		System.arraycopy(shortToAS400Bin2(sqlLen), 0, huddle, 2, 2);
	}

	public void setHex00(short hex00) {
		this.hex00 = hex00;
		System.arraycopy(shortToAS400Bin2(hex00), 0, huddle, 16, 2);
	}

	public void setCcsid(short ccsid) {
		this.ccsid = ccsid;
		log.debug("setting CCSID to: " + ccsid);
		System.arraycopy(shortToAS400Bin2(ccsid), 0, huddle, 18, 2);
	}

	public void setNameLength(short nameLength) {
		this.nameLength = nameLength;
		System.arraycopy(shortToAS400Bin2(nameLength), 0, huddle, 48, 2);
	}

	public void setName(String name) {
		this.name = name;
		System.arraycopy(stringToAS400Text(name, 30), 0, huddle, 50, 30);
	}

	public short getSqlType() {
		return sqlType;
	}

	public Object getObject() {
		Object o;
		if (isNull){
			o = null;
		}
		if (sqlType == 484) {
			o = aS400PackedDecimalToBigDecimal(shudder, sqlLen / 256,
					sqlLen % 256);
		} else if (sqlType == 488) {
			o = aS400ZonedDecimalToBigDecimal(shudder, sqlLen / 256,
					sqlLen % 256);
		} else if (sqlType == 492) {
			o = aS400Bin8ToLong(shudder);
		} else if (sqlType == 496) {
			o = aS400Bin4ToInt(shudder);
		} else if (sqlType == 500) {
			o = aS400Bin2ToShort(shudder);
		} else if (sqlType == 448){
			o = aS400TextToString(shudder).substring(2);
		} else if (sqlType == 456){
			o = aS400TextToString(shudder).substring(2);
		} else {
			o = aS400TextToString(shudder);
		}
		log.debug("Parameter Marker: " + o);
		return o;
	}

	public short getSqlLen() {
		return sqlLen;
	}

	public short getLengthOfShudder() {
		if ((sqlType % 2) == 1) {
			nullable = true;
			sqlType--;
		} else {
			nullable = false;
		}
		if (mudP == 0){
			return 0;
		}
		if (sqlType == 484) {
			return (short) ((sqlLen / 256 + 2) / 2);
		} else if (sqlType == 488)
			return (short) (sqlLen / 256);
		else {
			return sqlLen;
		}
	}

	public void setShudder(byte[] shudder) {
		this.shudder = shudder;
	}

	public void setIsNull(byte[] modder) {
		if (aS400Bin2ToShort(modder) < 0) {
			isNull = true;
		} else {
			isNull = false;
		}
	}

	public boolean isNullable() {
		return nullable;
	}

	public short getLengthOfShudder(byte[] work) {
		sqlLen = aS400Bin2ToShort(work);
		sqlLen += 2;
		return sqlLen;
	}

	public String getName() {
		return name;
	}

	public int getPrecision() {
		return sqlLen / 256;
	}

	public int getScale() {
		return sqlLen % 256;
	}

}
