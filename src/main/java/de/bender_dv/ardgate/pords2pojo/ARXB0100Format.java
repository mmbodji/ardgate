/* 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import java.sql.SQLException;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.ardgate.database.NotYetImplementedException;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * end of inherited fields
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 * end of inherited fields
 * 113	130	text	Default collection
 * 131	132	text	reserved
 * 133	136	int4	Offset to sqlda
 * 137	140	int4	Length of sqlda
 * 141	144	int4	Offset to SQL Statement
 * 145	148	int4	Length of SQL Statement
 * 149	152	int4	Offset to Declare Procedure
 * 153	156	int4	Length of Declare Procedure
 * 157	160	int4	Offset to Procedure name
 * 161	164	int4	Length of Procedure name
 * followed by binary huddle consisting of:
 * sqlda
 * shudder data belonging to sqlda
 * sql statement
 * declare procedure
 * procedure name
**/
public class ARXB0100Format extends GenericArxFormat {
	private static final Log log = LogFactory.getLog(ARXB0100Format.class);
	private String defaultCollection;
	private int offsetToSqlda;
	private int lengthOfSqlda;
	private int offsetToSqlStatement;
	private int lengthOfSqlStatement;
	private int offsetToDeclProc;
	private int lengthOfDeclProc;
	private int offsetToProcName;
	private int lengthOfProcName;
	private byte[] work;
	private String sqlStatement;
	private String declareProcedure;
	private String procedureName;
	private SqldaBean sqlda;
	private byte[] shudder;
	/**
	 * 
	 */
	public ARXB0100Format() {
	}

	/**
	 * @param huddle
	 */
	public ARXB0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		work = new byte[18];
		System.arraycopy(huddle, 112, work, 0, 18);
		defaultCollection = aS400TextToString(work);  
		log.debug("DefaultCollection: " + defaultCollection);
		offsetToSqlda = aS400Bin4ToInt(huddle, 132);
		log.debug("OffsetToSqlda: " + offsetToSqlda);
		lengthOfSqlda = aS400Bin4ToInt(huddle, 136);
		log.debug("LengthOfSqlda: " + lengthOfSqlda);
		offsetToSqlStatement = aS400Bin4ToInt(huddle, 140);
		log.debug("OffsetToSqlStatement: " + offsetToSqlStatement);
		lengthOfSqlStatement = aS400Bin4ToInt(huddle, 144);
		log.debug("LengthOfSqlStatement: " + lengthOfSqlStatement);
		offsetToDeclProc = aS400Bin4ToInt(huddle, 148);
		log.debug("OffsetToDeclProc: " + offsetToDeclProc);
		lengthOfDeclProc = aS400Bin4ToInt(huddle, 152);
		log.debug("LengthOfDeclProc: " + lengthOfDeclProc);
		offsetToProcName = aS400Bin4ToInt(huddle, 156);
		log.debug("OffsetToProcName: " + offsetToProcName);
		lengthOfProcName = aS400Bin4ToInt(huddle, 160);
		log.debug("LengthOfProcName: " + lengthOfProcName);

		// undocumented
		// shudder containing the content to fill in host variables
		// followed by content of null indicators
		// follows SQLDA
		// is followed by other components of huddle
		// originally the contents is pointered to the vars in sqlvar
		if (lengthOfSqlda > 0){
			int offsetToShudder = offsetToSqlda + lengthOfSqlda;
			int lengthOfShudder = huddle.length - offsetToShudder 
							- lengthOfSqlStatement - lengthOfDeclProc - lengthOfProcName;
			shudder = new byte[lengthOfShudder];
			System.arraycopy(huddle, offsetToShudder, shudder, 0, lengthOfShudder);
			log.debug(aS400TextToString(shudder));
			work = new byte[lengthOfSqlda];
			System.arraycopy(huddle, offsetToSqlda, work, 0, lengthOfSqlda);
			// end of shudder (not really, give me a bucket)
			sqlda = new SqldaBean(work, shudder, getCcsid());
		}
		work = new byte[lengthOfSqlStatement];
		System.arraycopy(huddle, offsetToSqlStatement, work, 0, lengthOfSqlStatement);
		sqlStatement = aS400TextToString(work);
		log.debug("SqlStatement: " + sqlStatement);
		
		work = new byte[lengthOfDeclProc];
		System.arraycopy(huddle, offsetToDeclProc, work, 0, lengthOfDeclProc);
		declareProcedure = aS400TextToString(work);
		log.debug("DeclareProcedure: " + declareProcedure);
		
		work = new byte[lengthOfProcName];
		System.arraycopy(huddle, offsetToProcName, work, 0, lengthOfProcName);
		procedureName = aS400TextToString(work);
		log.debug("ProcedureName: " + procedureName);
	}
	public int getNumberOfParameters(){
		if (sqlda == null){
			return 0;
		} else{
			return sqlda.getSqlD();
		}
	}
	public String getPrepareString() throws SQLException{
		String result;
		if (sqlStatement.contains(" INTO ") && 
				! sqlStatement.contains("INSERT ")){
			throw new NotYetImplementedException("Select into not supported", "58033", -969);
//		} else if (sqlStatement.contains(" CURRENT OF ")){
//			throw new NotYetImplementedException("positioned operations on Cursor not supported", "58033", -969);
		} else {
			result = sqlStatement.replace(": H", "?").replace("? ?", "?");
		}
		return result;
	}
	public Object getParameter(int i){
		return sqlda.getSqlVar(i);
	}
	public short getSqlType(int i){
		return sqlda.getSqlType(i);
	}

	public int getSqlLen(int i) {
		return sqlda.getSqlLen(i);
	}

	public int getPrecision(int i) {
		return sqlda.getPrecision(i);
	}

	public int getScale(int i) {
		return sqlda.getScale(i);
	}

	public SqldaBean getSqldaBean() {
		return sqlda;
	}
}
