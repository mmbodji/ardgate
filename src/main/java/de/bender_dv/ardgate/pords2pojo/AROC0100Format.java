/*
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * GenericArxFormat
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 * end of inherited fields
 * 113	130	text	Default Collection
 * 131	131	text	Blocking allowed
 * 132	132	text	reserved
 * 133	136	int4	Offset to SQLDA
 * 137	140	int4	Length of SQLDA
 * 141	144	int4	Offset to DECLARE CURSOR
 * 145	148	int4	Length of DECLARE CURSOR
 * 149	166	text	CursorName
 * followed by binary huddle
 * 					SQLDA
 * 					DECLARE CURSOR Statement
 * */
public class AROC0100Format extends GenericArxFormat {
	private static final Log log = LogFactory.getLog(AROC0100Format.class);
	private String defaultCollection;
	private String blockingAllowed;
	private String cursorName;
	private int offsetToSqlda;
	private int lengthOfSqlda;
	private int offsetToDeclareCursor;
	private int lengthOfDeclareCursor;
	private SqldaBean sqlda;
	private String declareCursor;
	private byte[] shudder;
	/**
	 * 
	 */
	public AROC0100Format() {
	}

	/**
	 * @param huddle
	 */
	public AROC0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		byte[] work = new byte[18];
		System.arraycopy(huddle, 112, work, 0, 18);
		defaultCollection = aS400TextToString(work);  
		log.debug("DefaultCollection: " + defaultCollection);
		
		work = new byte[1];
		System.arraycopy(huddle, 130, work, 0, 1);
		blockingAllowed = aS400TextToString(work);  
		log.debug("blockingAllowed: " + blockingAllowed);
		
		offsetToSqlda = aS400Bin4ToInt(huddle, 132);
		log.debug("offsetToSqlda: " + offsetToSqlda);

		lengthOfSqlda = aS400Bin4ToInt(huddle, 136);
		log.debug("lengthOfSqlda: " + lengthOfSqlda);

		offsetToDeclareCursor = aS400Bin4ToInt(huddle, 140);
		log.debug("offsetToDeclareCursor: " + offsetToDeclareCursor);

		lengthOfDeclareCursor = aS400Bin4ToInt(huddle, 144);
		log.debug("lengthOfDeclareCursor: " + lengthOfDeclareCursor);

		if (lengthOfSqlda > 0){
			int offsetToShudder = offsetToSqlda + lengthOfSqlda;
			int lengthOfShudder = huddle.length - offsetToShudder 
							- lengthOfDeclareCursor;
			shudder = new byte[lengthOfShudder];
			System.arraycopy(huddle, offsetToShudder, shudder, 0, lengthOfShudder);
			log.debug(aS400TextToString(shudder));
			work = new byte[lengthOfSqlda];
			System.arraycopy(huddle, offsetToSqlda, work, 0, lengthOfSqlda);
			
			sqlda = new SqldaBean(work, shudder, getCcsid());
		}
		work = new byte[lengthOfDeclareCursor];
		System.arraycopy(huddle, offsetToDeclareCursor, work, 0, lengthOfDeclareCursor);
		declareCursor = aS400TextToString(work);  
		log.debug("declareCursor: " + declareCursor);

		work = new byte[18];
		System.arraycopy(huddle, 148, work, 0, 18);
		cursorName = aS400TextToString(work);  
		log.debug("cursorName: " + cursorName);

	}

	public String getDefaultCollection() {
		return defaultCollection;
	}

	public String getBlockingAllowed() {
		return blockingAllowed;
	}

	public String getCursorName() {
		return cursorName;
	}

	public int getOffsetToSqlda() {
		return offsetToSqlda;
	}

	public int getLengthOfSqlda() {
		return lengthOfSqlda;
	}

	public int getOffsetToDeclareCursor() {
		return offsetToDeclareCursor;
	}

	public int getLengthOfDeclareCursor() {
		return lengthOfDeclareCursor;
	}

	public SqldaBean getSqlda() {
		return sqlda;
	}

	public String getDeclareCursor() {
		return declareCursor;
	}
	public int getNumberOfParameters(){
		if(lengthOfSqlda > 0){
			return sqlda.getSqlD();
		} else {
			return 0;
		}
	}
	public Object getParameter(int i){
		return sqlda.getSqlVar(i);
	}
}
