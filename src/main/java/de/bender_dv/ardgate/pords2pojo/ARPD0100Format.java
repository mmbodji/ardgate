/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * ARXI0100Format
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 * 113	116	int4	Offset to following huddle
 * 117	120	int4	length of following huddle
 * 			text	SQL statement
 * end of inherited fields
 * 121	138	text	Statement name	
 * */
public class ARPD0100Format extends ARXI0100Format {
	private static final Log log = LogFactory.getLog(ARPD0100Format.class);
	private String statementName;
	/**
	 * 
	 */
	public ARPD0100Format() {
	}

	/**
	 * @param huddle
	 */
	public ARPD0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		byte[] work = new byte[18];
		System.arraycopy(huddle, 120, work, 0, 18);
		statementName = aS400TextToString(work);  
		log.debug("StatementName: " + statementName);	
	}

	public String getStatementName() {
		return statementName;
	}

}
