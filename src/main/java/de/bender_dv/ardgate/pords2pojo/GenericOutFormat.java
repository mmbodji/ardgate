/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 *	just for inheritance (might be interface?)
 */
public abstract class GenericOutFormat extends DataStructure implements OutFormat{

	/**
	 * 
	 */
	public GenericOutFormat() {
		// TODO Auto-generated constructor stub
	}
	public abstract int length();
	public abstract byte[] getHuddle();
}
