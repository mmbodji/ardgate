/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * 
 * */
public class QTNRIFormat extends GenericOutFormat{
	private static final Log log = LogFactory.getLog(QTNRIFormat.class);
	private byte[] huddle;
	private int length;
	private String commitVote;
	private String classifyResult;
	private String changesEnded;
	/**
	 * 
	 */
	public QTNRIFormat() {
		// TODO Auto-generated constructor stub
		super();
		huddle = new byte[7];
		setLength(7);	
		setChangesEnded("0");
		setClassifyResult("0");
	}

	public QTNRIFormat(String commitVote) {
		this();
		setCommitVote(commitVote);
	}

	/**
	 * @param huddle
	 */
	public QTNRIFormat(byte[] huddle, int ccsid) {
		super();
		huddle = new byte[8];
	}

	public byte[] getHuddle() {
		return huddle;
	}

	protected void setLength(int length) {
		System.arraycopy(intToAS400Bin4(length), 0, huddle, 0, 4);
		this.length = length;
	}

	public void setCommitVote(String commitVote) {
		this.commitVote = commitVote;
		huddle[4] = stringToAS400Text(commitVote, 1)[0];
	}

	public void setClassifyResult(String classifyResult) {
		this.classifyResult = classifyResult;
		huddle[5] = stringToAS400Text(classifyResult, 1)[0];
	}

	public void setChangesEnded(String changesEnded) {
		this.changesEnded = changesEnded;
		huddle[6] = stringToAS400Text(changesEnded, 1)[0];
	}

	public int length() {
		return huddle.length;
	}

}
