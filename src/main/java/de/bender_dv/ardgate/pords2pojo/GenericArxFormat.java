/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * end of inherited fields
 * GenericARCFormat
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * end of inherited fields
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 **/
public abstract class GenericArxFormat extends GenericARCFormat {
	private static final Log log = LogFactory.getLog(GenericArxFormat.class);
	private String stringDelimiter;
	private String decimalDelimiter;
	private String dateFormat;
	private String timeFormat;
	private String isolationLevel;
	/**
	 * 
	 */
	public GenericArxFormat() {
	}

	/**
	 * @param huddle
	 */
	public GenericArxFormat(byte[] huddle, int ccsid) {
		super(huddle, ccsid);
		
		byte[] work = new byte[1];
		System.arraycopy(huddle, 80, work, 0, 1);
		stringDelimiter = aS400TextToString(work);  
		log.debug("stringDelimiter: " + stringDelimiter);		

		System.arraycopy(huddle, 81, work, 0, 1);
		decimalDelimiter = aS400TextToString(work);  
		log.debug("decimalDelimiter: " + decimalDelimiter);		

		work = new byte[10];
		System.arraycopy(huddle, 82, work, 0, 10);
		dateFormat = aS400TextToString(work);  
		log.debug("dateFormat: " + dateFormat);		

		System.arraycopy(huddle, 92, work, 0, 10);
		timeFormat = aS400TextToString(work);  
		log.debug("timeFormat: " + timeFormat);		

		System.arraycopy(huddle, 102, work, 0, 10);
		isolationLevel = aS400TextToString(work);  
		log.debug("isolationLevel: " + isolationLevel);		

	}


	public String getStringDelimiter() {
		return stringDelimiter;
	}

	public String getDecimalDelimiter() {
		return decimalDelimiter;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public String getIsolationLevel() {
		return isolationLevel;
	}

}
