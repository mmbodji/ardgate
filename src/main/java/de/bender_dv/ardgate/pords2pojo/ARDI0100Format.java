/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * end of inherited fields
 * 27	28	text	Reserved
 * 29	32	int4	Disconnect type
 * 
 * */
public class ARDI0100Format extends GenericArFormat {
	private static final Log log = LogFactory.getLog(ARDI0100Format.class);
	private String reserved;
	private int disconnectType;
	/**
	 * 
	 */
	public ARDI0100Format() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param huddle
	 */
	public ARDI0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		byte[] work = new byte[2];
		System.arraycopy(huddle, 26, work, 0, 2);
		reserved = aS400TextToString(work);  
		log.debug("reserved: " + reserved);	

		disconnectType = aS400Bin4ToInt(huddle, 28);
		log.debug("disconnectType: " + disconnectType);
	}

	public int getDisconnectType() {
		return disconnectType;
	}
}
