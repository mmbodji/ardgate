/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * 1	3	text	Server product
 * 4	5	text	Server version
 * 6	7	text	Server release
 * 8	8	text	Server level
 * 9	18	text	UserId
 * 19	19	text	Include bound statements
 * 20	20	text	Protected conversation	
 */
public class OutConnectFormat extends GenericOutFormat {
	private static final Log log = LogFactory.getLog(OutConnectFormat.class);
	private byte[] huddle;
	private String product;
	private String version;
	private String release;
	private String level;
	private String userId;
	private String includeStatements;
	private String protectedConversation;
	/**
	 * 
	 */
	public OutConnectFormat() {
		super();
	}
	public OutConnectFormat(String userId) {
		huddle = new byte[20];
		setProduct("D*B");
		setVersion("01");
		setRelease("01");
		setLevel("0");
		setUserId(userId);
		setIncludeStatements("1");
		setProtectedConversation("0");
	}	
	public byte[] getHuddle() {
		return huddle;
	}
	public void setProduct(String product) {
		this.product = product;
		System.arraycopy(stringToAS400Text(product, 3), 0, huddle, 0, 3);
	}
	public void setVersion(String version) {
		this.version = version;
		System.arraycopy(stringToAS400Text(version, 2), 0, huddle, 3, 2);
	}
	public void setRelease(String release) {
		this.release = release;
		System.arraycopy(stringToAS400Text(release, 2), 0, huddle, 5, 2);
	}
	public void setLevel(String level) {
		this.level = level;
		System.arraycopy(stringToAS400Text(level, 1), 0, huddle, 7, 1);
	}
	public void setUserId(String userId) {
		this.userId = userId;
		System.arraycopy(stringToAS400Text(userId, 10), 0, huddle, 8, 10);
	}
	public void setIncludeStatements(String includeStatements) {
		this.includeStatements = includeStatements;
		System.arraycopy(stringToAS400Text(includeStatements, 1), 0, huddle, 18, 1);
	}
	public void setProtectedConversation(String protectedConversation) {
		this.protectedConversation = protectedConversation;
		System.arraycopy(stringToAS400Text(protectedConversation, 1), 0, huddle, 19, 1);
	}
	public int length() {
		return 20;
	}
}
