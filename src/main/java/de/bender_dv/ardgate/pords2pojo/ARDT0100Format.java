/* 
 * File: ARDT0100Format.java 
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur with variable length
 * from	to	type
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * end of inherited fields
 * 27	28	text	reserved
 * 29	32	int4	CCSID
 * 33	36	int4	Offset to object name
 * 37	40	int4	Length of object name
 *			text	Object name
 */
public class ARDT0100Format extends GenericArFormat {
	private static final Log log = LogFactory.getLog(ARDT0100Format.class);
	private int ccsid;
	private int offset;
	private int length;
	private String name;
	public ARDT0100Format() {
		super();
	}
	public ARDT0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);
		this.ccsid = aS400Bin4ToInt(huddle, 28);
		log.debug("ccsid: " + ccsid);
		offset = aS400Bin4ToInt(huddle, 32);
		log.debug("offset: " + offset);
		length = aS400Bin4ToInt(huddle, 36);
		log.debug("length: " + length);
		byte[] work = new byte[length];
		System.arraycopy(huddle, offset, work, 0, length);
		name = this.aS400TextToString(work);
		log.debug("name: " + name);	
	}
	public int getCcsid() {
		return ccsid;
	}
	public int getOffset() {
		return offset;
	}
	public int getLength() {
		return length;
	}
	public String getName() {
		return name;
	}
}
