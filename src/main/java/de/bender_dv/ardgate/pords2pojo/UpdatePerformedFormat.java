/**
 * 
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur
 * from	to	type
 * 1	1	text	Update ocurred 0 false 1 true 
 * 
 */
public class UpdatePerformedFormat extends GenericOutFormat {
	private static final Log log = LogFactory.getLog(UpdatePerformedFormat.class);
	private byte[] huddle;
	private String updateOcurred;
	/**
	 * 
	 */
	public UpdatePerformedFormat() {
		super();
		huddle = new byte[1];
	}
	public UpdatePerformedFormat(String updateOcurred) {
		this();
		setUpdateOcurred(updateOcurred);
	}
	public void setUpdateOcurred(String updateOcurred) {
		this.updateOcurred = updateOcurred;
		System.arraycopy(stringToAS400Text(updateOcurred, 1), 0, huddle, 0, 1);
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.pords2pojo.GenericOutFormat#getHuddle()
	 */
	public byte[] getHuddle() {
		// TODO Auto-generated method stub
		return huddle;
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.ardgate.pords2pojo.GenericOutFormat#length()
	 */
	public int length() {
		// TODO Auto-generated method stub
		return huddle.length;
	}

}
