/**
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.ardgate.pords2pojo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Bender
 * binary huddle, representing DataStructur variable length
 * from	to	type
 * GenericArFormat
 * 1	4	int4	Output format buffer size
 * 5	8	int4	Activation group number
 * 9	26	text	RDB name
 * GenericArxFormat
 * 27	44	text	Package collection
 * 45	62	text	Package name
 * 63	70	text	Package consistency token
 * 71	72	text	reserved
 * 73	76	int4	Section number
 * 77	80	int4	CCSID
 * 81	81	text	String delimiter
 * 82	82	text	Decimal delimiter
 * 83	92	text	Date format
 * 93	102	text	Time format
 * 103	112	text	Isolation level
 * end of inherited fields
 * 113	116	int4	Offset to following huddle
 * 117	120	int4	length of following huddle
 * 121	124	int4	Offset to following huddle
 * 125	128	int4	length of following huddle
 * 129	146	text	Statementname
 * 			text	SQLDA huddle
 * 			text	shudder
 * 			text	Procedure name
 * */
public class ARXP0100Format extends GenericArxFormat {
	private static final Log log = LogFactory.getLog(ARXP0100Format.class);
	private int offsetToSqlda;
	private int lengthOfSqlda;
	private int offsetToProcName;
	private int lengthOfProcName;
	private String statementName;
	private byte[] shudder;
	private SqldaBean sqlda;
	private String procName;
	/**
	 * 
	 */
	public ARXP0100Format() {
	}

	/**
	 * @param huddle
	 */
	public ARXP0100Format(byte[] huddle, int ccsid) {
		super(huddle, ccsid);

		byte[] work;

		offsetToSqlda = aS400Bin4ToInt(huddle, 112);
		log.debug("offsetToSqlda: " + offsetToSqlda);

		lengthOfSqlda = aS400Bin4ToInt(huddle, 116);
		log.debug("lengthOfSqlda: " + lengthOfSqlda);

		offsetToProcName = aS400Bin4ToInt(huddle, 120);
		log.debug("offsetToProcName: " + offsetToProcName);

		lengthOfProcName = aS400Bin4ToInt(huddle, 124);
		log.debug("lengthOfProcName: " + lengthOfProcName);

		work = new byte[18];
		System.arraycopy(huddle, 128, work, 0, 18);
		statementName = aS400TextToString(work);  
		log.debug("StatementName: " + statementName);	
		if (lengthOfSqlda > 0){
			int offsetToShudder = offsetToSqlda + lengthOfSqlda;
			int lengthOfShudder = huddle.length - offsetToShudder 
							- lengthOfProcName;
			shudder = new byte[lengthOfShudder];
			System.arraycopy(huddle, offsetToShudder, shudder, 0, lengthOfShudder);
			log.debug("shudder: " + aS400TextToString(shudder));
			work = new byte[lengthOfSqlda];
			System.arraycopy(huddle, offsetToSqlda, work, 0, lengthOfSqlda);
			
			sqlda = new SqldaBean(work, shudder, getCcsid());	
		}
		if(lengthOfProcName > 0){
			work = new byte[lengthOfProcName];
			System.arraycopy(huddle, offsetToProcName, work, 0, lengthOfProcName);
			procName = aS400TextToString(work);
		}
	}
	public String getStatementName() {
		return statementName;
	}
	public String getProcName() {
		return procName;
	}

	public int getNumberOfParameters(){
		if(sqlda != null){
			return sqlda.getSqlD();			
		}
		else{
			return 0;			
		}
	}
	public Object getParameter(int i){
		return sqlda.getSqlVar(i);
	}
}
