package de.bender_dv.as400.datastructure;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.BinaryConverter;

public class Converter {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(Converter.class);
	private int convCcsid;
	public Converter() {
		this(500);
	}

	public Converter(int ccsid) {
		super();
		this.convCcsid = ccsid;
		log.debug("convCcsid: " + convCcsid);
	}

	public void setConvCcsid(int ccsid) {
		this.convCcsid = ccsid;
	}

	public String aS400TextToString(byte[] text, int ccsid) {
		return (String) new AS400Text (text.length, ccsid).toObject(text);
	}
	public String aS400TextToString(byte[] text) {
		return aS400TextToString(text, convCcsid);
	}
	public short aS400Bin2ToShort(byte[] value){
		return BinaryConverter.byteArrayToShort(value, 0);
	}
	public short aS400Bin2ToShort(byte[] value, int start){
		return BinaryConverter.byteArrayToShort(value, start);
	}
	public int aS400Bin4ToInt(byte[] value){
		return BinaryConverter.byteArrayToInt(value, 0);
	}
	public int aS400Bin4ToInt(byte[] value, int start){
		return BinaryConverter.byteArrayToInt(value, start);
	}
	public long aS400Bin8ToLong(byte[] value){
		return BinaryConverter.byteArrayToLong(value, 0);
	}
	public byte[] stringToAS400Text(String text, int length, int ccsid) {
		if (text.length() > length){
			text = text.substring(0, length - 1);
		}
		byte[] result = new byte[length];
		new AS400Text(length, ccsid).toBytes(text, result);
		return result;
	}	
	public byte[] stringToAS400Text(String text, int length) {
		return stringToAS400Text(text, length, convCcsid);
	}
	public byte[] stringToAS400Text(String text) {
		return stringToAS400Text(text, text.length(), convCcsid);
	}
	public byte[] stringToAS400VarText(String text){
		return stringToAS400VarText(text, text.length());
	}
	public byte[] stringToAS400VarText(String text, int length, int ccsid){
		byte[] huddle = new byte[length + 2];
		System.arraycopy(shortToAS400Bin2((short) length), 0, 
				huddle, 0, 2);
		System.arraycopy(stringToAS400Text(text, ccsid), 0, 
				huddle, 2, length);

		return huddle;
	}
	public byte[] stringToAS400VarText(String text, int length){
		return stringToAS400VarText(text, length, convCcsid);
	}
	public byte[] shortToAS400Bin2(short value){
		return BinaryConverter.shortToByteArray(value);
	}
	public byte[] intToAS400Bin4(int value){
		return BinaryConverter.intToByteArray(value);
	}
	public byte[] longToAS400Bin8(long value){
		return BinaryConverter.longToByteArray(value);
	}
	public byte[] bigDecimalToAS400ZonedDecimal(BigDecimal number, int precision, int scale){
		return new AS400ZonedDecimal(precision, scale).toBytes(number);
	}
	public byte[] bigDecimalToAS400PackedDecimal(BigDecimal number, int precision, int scale){
		return new AS400PackedDecimal(precision, scale).toBytes(number);
	}
	public BigDecimal aS400PackedDecimalToBigDecimal(byte[] huddle, int precision, int scale){
		return (BigDecimal) new AS400PackedDecimal(precision, scale).toObject(huddle);
	}
	public BigDecimal aS400ZonedDecimalToBigDecimal(byte[] huddle, int precision, int scale){
		return (BigDecimal) new AS400ZonedDecimal(precision, scale).toObject(huddle);
	}
}