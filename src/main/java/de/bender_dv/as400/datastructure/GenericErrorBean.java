/* 
 * File: GenericErrorBean.java used by AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.as400.datastructure;

import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.RecordFormat;

/**
 * might be moved to package communication and disappear from public scope
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:15:26 $
 */
public class GenericErrorBean extends DataStructure {
	private static final Log log = LogFactory.getLog(GenericErrorBean.class);

	private AS400Record record;
	private static RecordFormat format = new RecordFormat();
	static{
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(50, 273), "MESSAGE"));
	}
	public GenericErrorBean() {
		super(500);
		record = new AS400Record(format);
		this.setMESSAGE("no such handler");
	}
	public GenericErrorBean(byte[] contents) throws UnsupportedEncodingException {
		super(500);
		log.debug("<CONTENTS>" + contents + "</CONTENTS>");		
		record = new AS400Record(format, contents);
		log.debug("created success");
	}

	public void setRecord(byte[] rec) 
	throws UnsupportedEncodingException{
		log.debug("<REC>" + rec + "</REC>");		
		record.setContents(rec);
		log.debug("setRecord success");		
	}
	public byte[] getRecord(){
		return record.getContents();
	}
	public String getMESSAGE() {
		return (String) record.getField("MESSAGE");
	}

	public void setMESSAGE(String source) {
		try {
			record.setField("MESSAGE", source);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
