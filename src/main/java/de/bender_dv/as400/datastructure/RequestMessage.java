/* 
 * File: RequestMessage.java used for internal Communication AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.as400.datastructure;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * only usefull for internal use of AppServer4RPG
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:15:36 $
 */
public class RequestMessage extends DataStructure {
	private static final Log log = LogFactory.getLog(RequestMessage.class);

	private byte[] data;
	private String text;
	private String responseqName;
	private String eventName;
	public RequestMessage() {
		super(500);
	}
	public RequestMessage(byte[] data) {
		this();
		this.data = data;
		log.debug("created " + data);
	}	
	public RequestMessage(byte[] data, String text) {
		this();
		this.data = data;
		this.text = text;
		this.responseqName = text.substring(0, 10);
		this.eventName = text.substring(10, 20);

		log.debug("created " + text);
		log.debug("responseQ " + responseqName);
		log.debug("event " + eventName);
		
	}
	public String getResponseqName(){
		return this.responseqName;
	}
	public String getEventName() {
		return this.eventName;
	}
	public byte[] getEventData() {
		int l = data.length - 20;
		byte[] t = new byte[l];
		System.arraycopy(data, 20, t, 0, l);
		log.debug("eventData " + t);
		return t;
	}
}
