/* 
 * File: DataStructure.java generic DataStructure AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.as400.datastructure;



/**
 * Should be extended by your Hybrid Objects
 * not yet implemented
 * @author Dieter Bender
 * @version $Revision: 1.6 $ $Date: 2010/08/03 11:11:49 $
 */
public class DataStructure extends Converter{

	/**
	 * @deprecated Use {@link #DataStructure(int)} instead
	 */
	public DataStructure() {
		this(500);
	}

	public DataStructure(int ccsid) {
		super(ccsid);
	}
}
