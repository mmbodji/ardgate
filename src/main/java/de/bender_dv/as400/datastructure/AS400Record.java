/* 
 * File: AS400Record.java wrapper for Toolbox class Record AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.as400.datastructure;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;
/**
 * Wrapper for toolbox class Record 
 * to change behaviour of Record from rpg like to java like
 * not yet finished 
 * 
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:15:06 $
 */
public class AS400Record{

	private Record record;

	public AS400Record(RecordFormat format){
		record = new Record(format);
	}
	/**
	 * later it should check if the contents could be converted to java fields
	 * @param format
	 * @param contents
	 * @throws java.io.UnsupportedEncodingException
	 */
	public AS400Record(RecordFormat format, byte[] contents) 
	throws java.io.UnsupportedEncodingException{
		record = new Record(format, contents); 
	}
	/**
	 * validity check should be done by setField
	 * 
	 * @param name of field
	 * @return the field as Object
	 */
	public Object getField(String name) {
		Object result = null;
		try {
			result = record.getField(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * not yet finished, should check for validity
	 * @param name of field
	 * @param value
	 */
	public void setField(String name, Object value) 
	throws UnsupportedEncodingException	{
		record.setField(name, value);
	}
	/**
	 * @return PORDS (plain old RPG Datastructure)
	 */
	public byte[] getContents(){
		byte[] result = null;
		try{
			result = record.getContents();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * @param rec
	 * @throws UnsupportedEncodingException 
	 */
	public void setContents(byte[] rec) 
	throws UnsupportedEncodingException {
		record.setContents(rec);
		
	}
}
