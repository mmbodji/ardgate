package de.bender_dv.as400.datastructure;

public class SimpleMessage extends DataStructure {
	private String mESSAGE;
	private byte[] record;
	public SimpleMessage() {
		super(500);
	}
	public SimpleMessage(String message) {
		super(500);
		this.mESSAGE = message;
		setMESSAGE(message);
	}
	public String getMESSAGE() {
		return this.mESSAGE;
	}
	public byte[] getRecord() {
		return record;
	}
	public void setMESSAGE(String message) {
		this.mESSAGE = message;
		this.record = stringToAS400Text(message);
	}
	public void setRecord(byte[] record) {
		this.record = record;
		this.mESSAGE = aS400TextToString(record);
	}

}
