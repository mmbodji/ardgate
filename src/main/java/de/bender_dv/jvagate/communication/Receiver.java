/* 
 * File: Receiver.java listens at the incoming Q AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */package de.bender_dv.jvagate.communication;

import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.DataQueue;
import com.ibm.as400.access.DataQueueEntry;
import com.ibm.as400.access.ObjectAlreadyExistsException;

import de.bender_dv.as400.datastructure.RequestMessage;

/**
 * @author bender
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Receiver {

	private static final Log log = LogFactory.getLog(Receiver.class);
	private String system;	
	private String user;
	private String password;
	private String dataqPath;
	private Context context;

	public static void main(String[] args) {
		new Receiver().doWork();
	}

	/**
	 * 
	 */
	private AS400 as400;
	private DataQueue orderQ;
	private boolean shutDown = false;
	private ExecutorService executor;
	Receiver() {
		super();
		context = Context.getInstance();
		context.put("EventContainer", new HandlerContainer());
		ResourceBundle props = (ResourceBundle) context.get("GlobalProps");
		try {
			system = props.getString("as400.system").trim();
			user = props.getString("as400.user").trim();
			password = props.getString("as400.password").trim();
			dataqPath = props.getString("as400.dataqPath").trim();
			log.info("GlobalProps loaded");
		} catch (Exception e) {
			log.error("failed to load GlobalProps");
			e.printStackTrace();
		}
		executor = Executors.newCachedThreadPool();
		as400 = new AS400(system, user, password);
		log.debug("connected to " + as400);
		orderQ = new DataQueue(as400, dataqPath);
		log.info("connected to DTAQ " + orderQ);
	}
	void doWork(){
		try {
			try{orderQ.delete();} catch(Throwable t){}
			orderQ.create(532);
			while(! shutDown){
				log.info("getOrder");
				getOrder();
			}
		} catch (ObjectAlreadyExistsException e) {
			log.error("Dataq exists - Server was already running");
		} catch (Exception e) {
			log.error("unable to create Dataq");
			e.printStackTrace();
		}
		return;
	}

	/**
	 * 
	 */
	private void getOrder() {
		DataQueueEntry entry;
		Sender sender;
		RequestMessage entryData;
		String responseqName;
		String event;
		String parmString;
		try {
			entry = orderQ.read(-1);
			entryData = new RequestMessage(entry.getData(), entry.getString());
			log.debug("entry retrieved " + entryData);
			responseqName = entryData.getResponseqName();
			log.debug("responseQ " + entryData);
			sender = new Sender(as400, entryData);
			log.debug(entry.getString());
			executor.execute(sender);
			//new Thread(sender).start();
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		} 
	}
	protected void finalize() throws Throwable{
		try {
			log.debug("finalize");
			orderQ.delete();
		} finally{
			super.finalize();
		}
	}
}
