/* 
 * File: Context.java global Context AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.communication;

import java.util.Hashtable;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.jvagate.config.JvagateProperties;
/** 
 * Global Context AppServer4RPG, there will be only one instance per JVM.
 * 
 * @author Dieter Bender
 * @version $Revision: 1.6 $ $Date: 2010/12/20 13:24:00 $ 
 */
public final class Context extends Hashtable{
	private static Context instance;
	private static final Log log = LogFactory.getLog(Context.class);
	private static final long serialVersionUID = 8736447541784368169L;
	static{
		instance = new Context();
	}
	/**
	 * Use getInstance instead of Constructor
	 * 
	 * @return Context
	 */
		public static Context getInstance(){
			return instance;
		}
	private int ccsid;
	private String dataqPath;
	private String password;
	private Hashtable sessionContainer;
	private String system;	
	private String user;
	private Context() {
		super();
		sessionContainer = new Hashtable();
		ResourceBundle props = ResourceBundle.getBundle("global");
		this.put("GlobalProps", props);
		JvagateProperties jvaProps = new JvagateProperties(props);
		this.put("GlobalProperties", jvaProps);
		try {
			system = props.getString("as400.system");
			user = props.getString("as400.user");
			password = props.getString("as400.password");
			dataqPath = props.getString("as400.dataqPath");
			ccsid = Integer.parseInt(props.getString("as400.ccsid"));
			log.debug("GlobalProps loaded");
		} catch (Exception e) {
			log.error("failed to load GlobalProps");
			e.printStackTrace();
		}
	}
	public int getCcsid() {
		return ccsid;
	}
	public String getDataqPath() {
		return dataqPath;
	}
	public String getPassword() {
		return password;
	}
	Session getSession(String returnQ){
		Session session;
		session = (Session) sessionContainer.get(returnQ);
		if(session == null){
			session = new Session();
			sessionContainer.put(returnQ, session);
		}
		else{
			session.setNewFlag(false);
		}
		return session;
	}
	public String getSystem() {
		return system;
	}
	public String getUser() {
		return user;
	}
	void invalidateSession(String key){
		sessionContainer.remove(key);
	}
}
