/* 
 * File: Response.java used to send data back to JVAGATE AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.Converter;

/**
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:16:11 $ 
 */
public class Response {
	private static final Log log = LogFactory.getLog(Request.class);
	private byte[] message;
	Response(){
		super();
	}
	public Response(byte[] message){
		super();
		this.message = message;
	}
	public Response(String message){
		super();
		this.message = new Converter().stringToAS400Text(message);
	}	
	/**
	 * is used to send data back to JVAGATE. Use Hybrid Objects to convert data
	 * to PORDS (plain old RPG datastructure), don't spread binary data all over your 
	 * java application
	 * @see de.bender_dv.test.AS400Kunde
	 * @param message 
	 */
	public void setResultData(byte[] message){
		this.message = message;
		return;
	}
	byte[] getResponse(){
		return message;
	}
}
