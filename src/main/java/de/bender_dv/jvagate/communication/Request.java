/* 
 * File: Request.java used for data transport to EventHandler AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.communication;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * The request is used as data transport container from JVAGATE to the EventHandler, 
 * it is created by the Sender Object
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:16:11 $ 
 */
public class Request {
	private static final Log log = LogFactory.getLog(Request.class);
	private String event;
	private Session session;
	private byte[] message;
	private Vector packetCollector;
	private int packetCount;
	private int totalLength;

	Request() {
		super();
		this.packetCollector = new Vector();
	}
	Request(Session session) {
		super();
		this.session = session;
		this.packetCollector = new Vector();
	}	
	Request(String event, Session session) {
		super();
		this.event = event;
		this.session = session;
		this.packetCollector = new Vector();
	}	
	Request(String event, Session session, byte[] message) {
		super();
		this.event = event;
		this.session = session;
		this.message = message;
		this.packetCollector = new Vector();
	}
	/**
	 * 
	 * @return String: the name of the Event fired by JVAGATE
	 */
	public String getEvent() {
		return event;
	}
	/**
	 * 
	 * @return Session: a Container Object to store attributes 
	 */
	public Session getSession() {
		return session;
	}

	void setEvent(String event) {
		this.event = event;
		log.info("Event set to: " + event);
	}
	/**
	 * 
	 * @return eventData, containing the binary contents of the PORDS (plain old RPG
	 * DataStructure) sent by JVAGATE. Use Hybrid Object to transform PORDS to 
	 * POJO (plain old Java Object), don't spread byte[] all over your java application.
	 * @see de.bender_dv.test.AS400Kunde
	 */
	public byte[] getEventData(){
		int from = 0;
		byte[] packet;
		if (packetCount > 0){
			message = new byte[totalLength];
			for(int i = 0; i < packetCount; i++){
				packet = (byte[]) packetCollector.get(i);
				System.arraycopy(packet, 0, message, from, packet.length);
				from = from + packet.length;
			}
		}
		return message;
	}

	void setSession(Session session) {
		this.session = session;
	}
	void addPacket(byte[] packet)
	{
		packetCount = packetCount + 1;
		totalLength = totalLength + packet.length;
		packetCollector.add(packet);
		return;
	}
}
