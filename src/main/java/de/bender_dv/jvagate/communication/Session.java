/* 
 * File: Session.java Session Context of AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.communication;

import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * The Session is automatic created and represents the corresponding AS400 Job
 * using JVAGATE its for usage by EventHandler to hold the state living longer than 
 * the event 
 * @author Dieter Bender
 * @version $Revision: 1.6 $ $Date: 2010/08/03 11:10:43 $
 */
public class Session {
	private static final Log log = LogFactory.getLog(Session.class);
	private boolean newFlag;
	private Hashtable content;
	private HandlerContainer container;
	private Request request;
	Session() {
		super();
		content = new Hashtable();
		// just to have a default for CCSID
		content.put("CCSID", new Integer(500));
		container = new HandlerContainer();
		request = new Request();
		this.newFlag = true;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	void setNewFlag(boolean flag){
		newFlag = flag;
	}
/**
 * 
 * @return <b>true</b> first time session is used, afterwards <b>false</b>  
 */
	public boolean isNew() {
		boolean result = newFlag;
		setNewFlag(false);
		return result;
	}
	/**
	 * @return the container
	 */
	HandlerContainer getContainer() {
		return container;
	}
	/**
	 * @param container the container to set
	 */
	void setContainer(HandlerContainer container) {
		this.container = container;
	}
	/**
	 * 
	 * @param name used by set
	 * @return the object, <b>null</b> if nothing was stored with this key
	 */
	public Object getAttribute(String name) {
		setNewFlag(false);
		return content.get(name);
	}
	/**
	 * 
	 * @param name 
	 * @param attribute to store
	 */
	public void setAttribute(String name, Object attribute) {
		setNewFlag(false);
		content.put(name, attribute);
	}
	public void removeAttribute(String name){
		content.remove(name);
		return;
	}
}
