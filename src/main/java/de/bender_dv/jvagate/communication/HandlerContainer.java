/* 
 * File: HandlerContainer.java stores the EventHandlers of a Session AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.communication;

import java.util.Hashtable;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.jvagate.application.EventHandler;
import de.bender_dv.jvagate.application.GenericErrorHandler;

	class HandlerContainer{
	private static final Log log = LogFactory.getLog(HandlerContainer.class);
	private Context context;
	private Hashtable handler;
	HandlerContainer() {
		super();
		context = Context.getInstance();
		handler = new Hashtable();
	}
	EventHandler newHandler(String event){
		String handlerName = null; 
		EventHandler result = null;
		ResourceBundle props = (ResourceBundle) context.get("GlobalProps");
		try {
			handlerName = props.getString("handler." + event.trim());
			result = (EventHandler)
			Class.forName(handlerName).getConstructor().newInstance();
		} catch (Exception e) {
			log.error("failed to create EventHandler" + event);
			e.printStackTrace();
		}
		if (result == null){
			result = new GenericErrorHandler();
		}
		handler.put(event, result);
		return result;
	}
	EventHandler getEventHandler(String event){
		EventHandler h;
//		getSession(sessionName);
		log.debug("getEventHandler ");
		log.debug("Handler " + handler);
		log.debug("Event " + event);
		h = (EventHandler) handler.get(event);
		if (h == null){
			h = newHandler(event);
		}
		return h;
	}
}
