/* 
 * File: Sender.java worker Thread, created by Receiver AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */package de.bender_dv.jvagate.communication;

import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.DataQueue;

import de.bender_dv.as400.datastructure.RequestMessage;
import de.bender_dv.as400.datastructure.SimpleMessage;
import de.bender_dv.jvagate.application.EventHandler;

/**
 * @author Dieter Bender
 * @version $Revision: 1.9 $ $Date: 2011/10/20 15:30:49 $
 */
class Sender implements Runnable {
	private static final Log log = LogFactory.getLog(Sender.class);
	private static final int PACKET_SIZE = 512;
	private Context context;
	private Session session;
	private String responseqLib;
	private RequestMessage message;
	private AS400 as400;
	private String qName;
	private DataQueue responseQ;
	/**
	 * 
	 */
	Sender() {
		super();
		// TODO Auto-generated constructor stub
	}
	Sender(AS400 as400, RequestMessage entryData) {
		super();
		this.as400 = as400;
		this.message = entryData;
		qName = message.getResponseqName();
		context = Context.getInstance();
		session = context.getSession(qName);
		if(session.isNew()){
			// changed to attribute handlerContainer
			// session.setAttribute(qName, new HandlerContainer());
		}
		// TODO responseqLib in Context cachen
		ResourceBundle props = (ResourceBundle) context.get("GlobalProps");
		try {
			responseqLib = props.getString("as400.responseqLib");
			log.debug("GlobalProps loaded");
		} catch (Exception e) {
			log.error("failed to load GlobalProps");
			e.printStackTrace();
		}

		log.debug("Sender created");
		
	}
	public void run(){
		log.info("$$$$$ " + qName + " $$$$$");
		String event = message.getEventName();
		log.debug("event:" + event + ":");
		byte[] eventData = message.getEventData();
		log.info("Event " + event);
		Request req = session.getRequest();
		if (event.equals("EXIT      ")){
			context.invalidateSession(qName);
		}
		else if (event.equals("          ")){
			req.addPacket(eventData);
			// maybe caching or pooling shut be better
			responseQ = new DataQueue(as400, responseqLib + qName.trim() + ".DTAQ");
			log.debug("connected to " + responseQ);
			try {
				responseQ.write(new Response("quit").getResponse());
			} catch (Exception e) {
				log.error("send " + e);
				e.printStackTrace();
			}
		}
		else{
			HandlerContainer container = session.getContainer();
			log.debug("HandlerContainer " + container);
			EventHandler handler = container.getEventHandler(event);	
			req.setEvent(event);
			req.setSession(session);
			req.addPacket(eventData);
			Response res = new Response();
			handler.performWork(req, res);
			session.setRequest(new Request());
			send(res);
		}
	}
	private void send(Response res) {
		byte[] response;
		byte[] packet;
		byte[] terminator = new SimpleMessage("has more").getRecord();
		int start;
		response = res.getResponse();
		responseQ = new DataQueue(as400, responseqLib + qName.trim() + ".DTAQ");
		log.info("connected to " + responseQ);
		for(start = 0; start < response.length - 512; ){
			packet = new byte[PACKET_SIZE + 10];
			System.arraycopy(response, start, packet, 0, PACKET_SIZE);
			System.arraycopy(terminator, 0, packet, PACKET_SIZE, terminator.length);
			start = start + PACKET_SIZE;
			sendPacket(packet);
		}
		int length = response.length % PACKET_SIZE;
		if (length == 0){
			length += PACKET_SIZE;
		}
		packet = new byte[length];
		System.arraycopy(response, start, packet, 0, packet.length);
		sendPacket(packet);
	}
	private void sendPacket(byte[] packet) {
		try {
			responseQ.write(packet);
		} catch (Exception e) {
			log.error("send " + e);
			e.printStackTrace();
		}
	}
}