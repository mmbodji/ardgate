/* 
 * File: GenericErrorHandler.java used if an event is not configured AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.application;

import de.bender_dv.as400.datastructure.GenericErrorBean;
import de.bender_dv.jvagate.communication.Request;
import de.bender_dv.jvagate.communication.Response;
/**
 * used if an event has no EventHandler configured, this class might be 
 * not visible later on 
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:15:56 $
 */
public class GenericErrorHandler implements EventHandler {
	public void performWork(Request req, Response res) {
		res.setResultData( new GenericErrorBean().getRecord());
	}
}
