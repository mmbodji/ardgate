/* 
 * File: eventHandler.java Interface to implement by your EventHanldlers AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.jvagate.application;

import de.bender_dv.jvagate.communication.Request;
import de.bender_dv.jvagate.communication.Response;

/**
 * Write your own EventHandler for each Event fired by the ServiceProgram
 * JVAGATE by extending this Interface and putting your code to the performWork
 * method.
 *   
 * Add an entry to the handler section of the global.properties in your classpath, 
 * binding the eventname used by your RPG application to the fully qualified name
 * of your own EventHandler
 * @see de.bender_dv.test.Confirmer
 * @author Dieter Bender
 * @version $Revision: 1.5 $ $Date: 2010/07/22 16:15:48 $
 */
public interface EventHandler {
	/**
	 * Write your own EventHandler for each Event fired by the ServiceProgram
	 * JVAGATE by extending this Interface and putting your code to the performWork
	 * method.
	 * 
	 * Add an entry to the handler section of the global.properties in your classpath.
	 * Use Hybrid Objects to transform the PORDS (plain old RPG DataStructure) to 
	 * POJOs (plain old java objects), don't spread binary data and byte[] all over
	 * your java application, it makes it look like RPG
	 *
	 * @see de.bender_dv.test.AS400Kunde
	 * @see de.bender_dv.test.Confirmer
	 * @param req the request
	 * @param res the response
	 */
	public void performWork(Request req, Response res);
}
