package de.bender_dv.jvagate.config;

import java.io.*;
import java.net.*;
import java.util.*;
import de.bender_dv.jvagate.config.ConfigInputStream;
/**
 * 
 * <br>
 * 
 * This class is intended to write a configuration properties only.<br>
 * More information about a configuration property object can be found in <code>ConfigInputStream</code> javadoc.<br>
 *  
 *   This class extends <code>FilterOutputStream</code> so that can be used by <code>Properties.store</code>. <br>
 *   To avoid <code>*USRSPC</code> object corruption, the <code>write</code> methods of this class, remove comments that <code>Properties.store</code> 
 *   would like to add, and adds <code>\r</code> line separator to comform to <code>*CR</code> rule of <code>*USRSPC</code>.<br>  
 *   Because data to be put into the <code>*USRSPC</code> must be <code>EBCDIC</code> coded, internally is used an
 *   <code>OutputStreamWriter</code> object that encode while writing, the predefined <code>CharSet</code> name is <code>Cp500</code> that's a multinational
 *   <code>SBCS</code> codepage for <code>EBCDIC</code> so 
 *   code-page variant characters should not be used, to allow data to be read as national EBCDIC code-page encoded.<br>
 *
 *   A tipical usage of this class can be:<br>
 *   <blockquote>
 *   Properties config ...;
 *   ConfigOutputStream output = new ConfigOutputStream("/ardgate/configpath.properties");
 *   config.store(output, null);
 *   config.close();
 *   
 *   or
 *   
 *   Properties config ...;
 *   ConfigOutputStream output = new ConfigOutputStream(new FileOutputStream("/ardgate/configpath.properties"));
 *   config.store(output, null);
 *   config.close();
 *   
 *   or 
 *   
 *   Properties config ...;
 *   ConfigInputStream.setConfigProperties(config);
 *   
 *   </blockquote>
 * @see ConfigInputStream
 */
public class ConfigOutputStream extends FilterOutputStream {
	OutputStreamWriter ow;

	/**
	 * 
	 * @param out OutputStream that will be used to build the <code>OutputStreamWriter</code>, it may be any class that 
	 * implements <code>java.io.OutputStream</code> 
	 * as a <code>java.io.FileOutputStream</code> class.
	 */
	public ConfigOutputStream(OutputStream out) {
		super(out);
		try {
			ow = new OutputStreamWriter(out, "Cp500");
		} catch (Exception e) {
			throw new RuntimeException(e.toString());
		}
	}
	/**
	 * 
	 * @param config String - the absolute path of the configuration file, may be specified relative to the current working directory.
	 * @throws  FileNotFoundException as <code>FileOutputStream</code> constructor throws it
	 * @see java.io.FileOutputStream
	 */
	public ConfigOutputStream(String config) throws FileNotFoundException {
		this(new FileOutputStream(config));
	}

    /**
     * Write a portion of an array of bytes.
     *
     * @param  b  Array of bytes
     * @param  off   Offset from which to start writing bytes
     * @param  len   Number of bytes to write
     *
     * @exception  IOException  If an I/O error occurs
     */
	public void write(byte[] b, int off, int len) throws IOException {
		String str = new String(b, off, len);
		int off0 = str.indexOf(0);
		if (off0 < 0)  ow.write(trimComment(str));
		else ow.write(trimComment(str.substring(0, len)));
	}
    /**
     * Write an array of bytes.
     *
     * @param  b  Array of bytes to be written
     *
     * @exception  IOException  If an I/O error occurs
     */
	public void write(byte[] b) throws IOException {
		this.write(b, 0, b.length);
	}
    /**
     * Write a single character if &gt;0.  The character to be written is contained in
     * the 16 low-order bits of the given integer value; the 16 high-order bits
     * are ignored.
      *
     * @param b  int specifying a character to be written.
     * @exception  IOException  If an I/O error occurs
     */
	public void write(int b) throws IOException {
		if (b != 0) ow.write(b);
	}
    /**
     * Close the stream, flushing it first.  Once a stream has been closed,
     * further write() or flush() invocations will cause an IOException to be
     * thrown.  Closing a previously-closed stream, however, has no effect.
     *
     * @exception  IOException  If an I/O error occurs
     */
	public void close() throws IOException {
		ow.flush();
		ow.close();
	}
    /**
     * Flush the stream.
     *
     * @exception  IOException  If an I/O error occurs
     */
	public void flush() throws IOException {
		ow.flush();
	}
    /**
     * Get rid of comments and line-separator other that <code>\r</code>
     *
     * @exception  IOException  If an I/O error occurs
     */
	private String trimComment(String cmt) throws IOException {
		LineNumberReader lr = new LineNumberReader(new InputStreamReader(new ByteArrayInputStream(cmt.getBytes())));
		StringBuffer b = new StringBuffer();
		String line = lr.readLine();
		while (line != null) {
			if (!line.startsWith("#"))
				b.append(line).append('\r');
			line = lr.readLine();
		}
		return b.toString();
	}
    /**
     * Store a <code>Properties</code> object into the <code>ConfigInputStream.CONFIG_NAME</code> file, that must be found in class-path.
     *
     * @param  config  Properties to store in <code>ConfigInputStream.CONFIG_NAME</code> file, that must be found in class-path.
     * If <code>null</code> will just be ignored.  
     * @exception  IOException  If an I/O error occurs or the configuration file is not found.
     * @see ConfigInputStream#getConfigProperties()
     */
	public static void setConfigProperties(Properties config) throws IOException {
		if (config != null) {
			URL configurl = ConfigOutputStream.class.getClassLoader().getResource(ConfigInputStream.CONFIG_NAME);
			if (configurl == null) {
				File curdir = new File(System.getProperty("user.dir"));
				File prop = new File(curdir, ConfigInputStream.CONFIG_NAME);
				if (prop.exists()) {
					configurl = prop.toURI().toURL();
				}
			}
			if (configurl == null) throw new FileNotFoundException(ConfigInputStream.CONFIG_NAME);
			ConfigOutputStream output = new ConfigOutputStream(configurl.getPath());
			config.store(output, null);
			output.close();
		}
	}
}
