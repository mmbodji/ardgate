/* File: JvagateProperties.java
 * Copyright (c) 2010 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */

package de.bender_dv.jvagate.config;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JvagateProperties {
	private static final Log log = LogFactory.getLog(JvagateProperties.class);
	ResourceBundle props;

	JvagateProperties() {
		super();
	}
	public JvagateProperties(ResourceBundle props) {
		super();
		this.props = props;
	}
	public String getString(String key){
		return props.getString(key);
	}
	public Properties getProperties(String subKey){
		Properties p = new Properties();
		log.debug("subKey: " + subKey);
		for (Enumeration<String> e = props.getKeys(); e.hasMoreElements();){
			String s = e.nextElement();
			log.debug("key: " + s);
			if(s.startsWith(subKey)){
				String k = s.substring(subKey.length());
				log.debug("key: " + k);
				p.put(k, props.getString(s).trim());
			}
		}
		return p;
	}
}
