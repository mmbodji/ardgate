package de.bender_dv.jvagate.config;

import java.io.*;
import java.util.*;
import de.bender_dv.jvagate.config.ConfigOutputStream;

/**
 * <br>
 * This class is intended to read a configuration properties only.<br>
 * A configuration property is an object that contains the path to the java AppServer4RPG, 
 * and the library where the native iSeries application is installed.
 * So only 2 elements in the form <code>key=value</code> are required.
 * Keys must be:
 * <sl>
 * <li><code>config.lib</code> with the library upper-case name as value
 * <li><code>config.path</code> with the absolute path to the install directory as value
 * </sl>
 * <br>
 * These 2 element are contained in an <code>*USRSPC</code> object created in the library indicated at <code>config.lib</code>
 * variable.<br>
 * The <code>*USRSPC</code> name must be <code>ARDGATEPTH</code> and has to be linked to an IFS file 
 * in the directory indicated at <code>config.path</code> variable. The link name must be <code>configpath.properties</code><BR>
 * The <code>*USRSPC</code> object can be created and edited with <code>EDTF</code> iSerie command, e.g.<br>
 * <blockquote>
 * EDTF  STMF('/QSYS.LIB/&lt;config.lib&gt;.LIB/ARDGATEPTH.USRSPC')
 * </blockquote>
 * Once created a new link must be created either with <code>ADDLNK</code> iSeries command or <code>ln</code> QSH command, e.g.
 * <blockquote>
 * ADDLNK OBJ('/QSYS.LIB/&lt;config.lib&gt;.LIB/ARDGATEPTH.USRSPC') NEWLNK('&lt;config.path&gt;/configpath.properties') LNKTYPE(*SYMBOLIC)
 * </blockquote>                                   
 * Once created the link has been created, can be edited directly from <code>WRKLNK</code> panel using option <code>2</code> and displayed with option <code>5</code>.<br>
 * The final layout should look like:
 * <blockquote>
 * config.lib=ARDGDEV<br>        
 * config.path=/home/ardgate/
 * </blockquote>
 * Set line separator to <code>*CR</code> when editing the very first time (using option <code>15</code> inside the editing window),
 * next times the line separator will be recognized by <code>EDTF</code> and <code>DSPF</code> commands.
 *   
 *   <br>
 *   This class extends <code>FilterInputStream</code> so that can be used by <code>Properties.load</code>. <br>
 *   As an  <code>*USRSPC</code> object is filled up by binary zeros, the <code>read</code> methods of this class 
 *   stop reading at the first zero that is found. Because of data received are <code>EBCDIC</code> coded, internally is used a
 *   <code>InputStreamReader</code> object that decode while reading, the predefined <code>CharSet</code> name is <code>Cp500</code> that's a multinational
 *   <code>SBCS</code> codepage for <code>EBCDIC</code>, really the data are possibly encoded in a national EBCDIC code-page, so
 *   code-page variant characters should not be used.<br>
 *   
 *   A tipical usage of this class can be:<br>
 *   <blockquote>
 *   ConfigInputStream input = new ConfigInputStream("/ardgate/configpath.properties");
 *   Properties config = new Properties();
 *   config.load(input);
 *   config.close();
 *   
 *   or
 *   
 *   ConfigInputStream input = new ConfigInputStream(new FileInputStream("/ardgate/configpath.properties"));
 *   Properties config = new Properties();
 *   config.load(input);
 *   config.close();
 *   
 *   or 
 *   
 *   Properties config = ConfigInputStream.getConfigProperties();
 *   
 *   </blockquote>
 * @see ConfigOutputStream
 *   
 */
public class ConfigInputStream extends FilterInputStream {

	InputStreamReader ir;
	public static final String CONFIG_NAME = "configpath.properties";
	/**
	 * 
	 * @param in InputStream that will be used to build the <code>InputStreamReader</code>, it may be any class that implements <code>java.io.InputStream</code> 
	 * as a <code>java.io.FileInputStream</code> class.
	 */
	public ConfigInputStream(InputStream in) {
		super(in);
		try {
			ir = new InputStreamReader(in, "Cp500");
		} catch (Exception e) {
			throw new RuntimeException(e.toString());
		}
	}
	/**
	 * 
	 * @param config String - the absolute path of the configuration file, may be specified relative to the current working directory.
	 * @throws  FileNotFoundException as <code>FileInputStream</code> constructor throws it
	 * @see java.io.FileInputStream
	 */
	
	public ConfigInputStream(String config) throws FileNotFoundException {
		this(new FileInputStream(config));
	}

    /**
     * Read a single character.
     *
     * @return The character read, or -1 if the end of the stream has been
     *         reached or a binary zero is encountered.
     *
     * @exception  IOException  If an I/O error occurs
     */
	public int read() throws IOException {
		int b = ir.read();
		if (b == 0)
			return -1;
		return b;
	}
    /**
     * Read characters into an array.  This method will block until some input
     * is available, an I/O error occurs, or the end of the stream is reached.
     *
     * @param       b  Destination buffer
     *
     * @return      The number of characters read, or -1 
     *              if the end of the stream
     *              has been reached or a binary zero is encountered
     *
     * @exception   IOException  If an I/O error occurs
     */

	public int read(byte[] b) throws IOException {
		return this.read(b, 0, b.length);
	}

    /**
     * Read characters into a portion of an array.  This method will block
     * until some input is available, an I/O error occurs, or the end of the
     * stream is reached.
     *
     * @param      b  Destination buffer
     * @param      off   Offset at which to start storing characters
     * @param      len   Maximum number of characters to read
     *
     * @return     The number of characters read, or -1 if the end of the
     *             stream has been reached or a binary zero is encountered
     *
     * @exception  IOException  If an I/O error occurs
     */
	public int read(byte[] b, int off, int len) throws IOException {
		char[] cbuf = b == null ? new char[0] : new char[len];
		int br = ir.read(cbuf);
		if (br > 0) {
			String str = new String(cbuf, 0, br);
			int off0 = str.indexOf(0);
			br = (off0 < 0)?br:off0;
			System.arraycopy(str.getBytes(), 0, b, off, (len > br) ? br : len);
		}
		return br;
	}
	   /**
     * Close the stream.
     *
     * @exception  IOException  If an I/O error occurs
     */
	public void close() throws IOException {
		ir.close();
	}
    /**
     * Create a new <code>Properties</code> object and reads the <code>ConfigInputStream.CONFIG_NAME</code> file that must be found in class-path or current working directory.
     *
     * @exception  IOException  If an I/O error occurs or the configuration file is not found.
     * @see ConfigOutputStream#setConfigProperties(Properties)
     */
	public static Properties getConfigProperties() throws IOException {
		InputStream prop = ConfigInputStream.class.getClassLoader().getResourceAsStream(CONFIG_NAME);
		if (prop == null) {
			File curdir = new File(System.getProperty("user.dir"));
			File propf = new File(curdir, ConfigInputStream.CONFIG_NAME);
			if (propf.exists()) {
				prop = new FileInputStream(propf);
			}
		}
		if (prop == null) throw new FileNotFoundException(ConfigInputStream.CONFIG_NAME);
		Properties config = new Properties();
		ConfigInputStream input = new ConfigInputStream(prop);
		config.load(input);
		input.close();
		return config;
	}
}
