package de.bender_dv.commandgate;

import java.security.AccessControlException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

class ExitPreventer extends SecurityManager {
	private static final Log log = LogFactory.getLog(ExitPreventer.class);

	public void checkExit(int status) 
    {
        super.checkExit(status);
        log.debug("System.exit(" + status + ") caught");
        throw new AccessControlException("System.exit() not allowed");
    }
}
