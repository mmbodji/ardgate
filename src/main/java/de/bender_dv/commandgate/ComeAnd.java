/*
 * Copyright (c) 2013 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.commandgate;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ComeAnd {
	private static final Log log = LogFactory.getLog(ComeAnd.class);

	public static void run(String name, String[] parms) 
			throws Exception {
		Class myClass = Class.forName(name);
		log.info("Class name: " + name);
		Method myMethod = myClass.getMethod("main", new Class[] {String[].class});
		try{
			myMethod.invoke(null, new Object[]{parms});
		} catch (SecurityException e) {
			log.debug("I've caught you!");
		}		
		log.info("success");
		return;
	}
}
