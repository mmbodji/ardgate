/* 
 * File: Pords2PojoCommand.java
 * Copyright (c) 2013 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.commandgate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

public class Pords2PojoResponseBean extends DataStructure {
	public Pords2PojoResponseBean(int status, String message) {
		super(500);
		huddle = new byte[512];
		setStatus(status);
		setMessage(message);
	}
	public Pords2PojoResponseBean() {
		super(500);
	}
	public Pords2PojoResponseBean(int ccsid) {
		super(ccsid);
	}
	private static final Log log = LogFactory.getLog(Pords2PojoResponseBean.class);

	private int status;
	private String message;
	private byte[] huddle;
	public byte[] getHuddle() {
		return huddle;
	}
	public void setStatus(int status) {
		this.status = status;
		System.arraycopy(intToAS400Bin4(status) , 0 , huddle, 0 , 4);
	}
	public void setMessage(String message) {
		this.message = message;
		System.arraycopy(stringToAS400Text(message, 508), 0, huddle, 4, 508);
	}
}
