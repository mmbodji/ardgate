/* 
 * File: CommandHandler.java
 * Copyright (c) 2013 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.commandgate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.jvagate.application.EventHandler;
import de.bender_dv.jvagate.communication.Request;
import de.bender_dv.jvagate.communication.Response;

public class CommandHandler implements EventHandler {
	private static final Log log = LogFactory.getLog(CommandHandler.class);
//	static{
//		System.setSecurityManager(new ExitPreventer());
//	}
	public void performWork(Request req, Response res) {
		Pords2PojoResponseBean resBean;
		Pords2PojoCommandBean comBean;
		log.debug("Event RUNJAVARUN fired");
		comBean = new Pords2PojoCommandBean(req.getEventData());
		try {
			ComeAnd.run(comBean.getName(), comBean.getArgs());
			log.debug("success running " + comBean.getName());
			resBean = new Pords2PojoResponseBean(0, "success running " + comBean.getName());			
		} catch (SecurityException e) {
			log.debug("SecurityException caught");
			log.debug(e.getMessage());
			resBean = new Pords2PojoResponseBean(0, "success running " + comBean.getName());			
			//it's all the same to me
		} catch (Exception e) {
			resBean = new Pords2PojoResponseBean(-1, "" + e.getCause());
			log.debug("strange");
			log.debug(e.getMessage());
			e.printStackTrace();
		}
		log.debug("doing my work");
		res.setResultData(resBean.getHuddle());
	}

}
