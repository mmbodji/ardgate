/* 
 * File: Pords2PojoCommand.java
 * Copyright (c) 2013 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.commandgate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.as400.datastructure.DataStructure;

public class Pords2PojoCommandBean extends DataStructure {
	public Pords2PojoCommandBean(byte[] huddle) {
		super(500);
		setHuddle(huddle);
	}
	private static final Log log = LogFactory.getLog(Pords2PojoCommandBean.class);

	private String name;
	private String[] args;
	private byte[] huddle;
	public String getName() {
		return name;
	}
	public String[] getArgs() {
		return args;
	}
	public void setHuddle(byte[] huddle) {
		this.huddle = huddle;
		String work = aS400TextToString(huddle);
		log.debug(work);
		String[] workAry = work.split("\\s");
		name = workAry[0];
		log.debug("Class to call " + name);
		args = new String[workAry.length - 1];
		System.arraycopy(workAry, 1, args, 0, workAry.length - 1);
	}
}
