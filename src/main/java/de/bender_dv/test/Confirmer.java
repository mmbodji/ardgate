/* 
 * File: Confirmer.java example for AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */

package de.bender_dv.test;

import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.bender_dv.jvagate.application.EventHandler;
import de.bender_dv.jvagate.communication.Request;
import de.bender_dv.jvagate.communication.Response;

/**
 * 
 * 
 * @author Dieter Bender
 * @version $Revision: 1.7 $ $Date: 2010/09/23 17:22:58 $
 * 
 */
public class Confirmer implements EventHandler {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(Confirmer.class);
	public Confirmer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see de.bender_dv.sqlgateway.application.EventHandler#performWork(java.lang.String, java.lang.String)
	 */
	public void performWork(Request req, Response res) {
		byte[] eventData = req.getEventData();
		SqlstateBean result = null;
		result = new SqlstateBean();
		log.debug("event " + req.getEvent() + " data " + eventData);
		AS400Kunde kunde = null;
		try {
			kunde = new AS400Kunde(eventData);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			log.error("create of AS400Kunde failed" );
			e.printStackTrace();
		}
		log.debug("KundeId: " + kunde.getKUNDE_ID());
		log.debug("Vorname: " + kunde.getVORNAME());
		log.debug("Nachname: " + kunde.getNACHNAME());
		log.debug("Stra�e: " + kunde.getSTRASSE());
		log.debug("PLZ: " + kunde.getPLZ());
		log.debug("Ort: " + kunde.getORT());
		log.debug("Telefon: " + kunde.getTEL());
		log.debug("Fax: " + kunde.getFAX());
		log.debug("EMail: " + kunde.getEMAIL());
		result.setSQLSTATE(100);
		res.setResultData(result.getRecord());
		return;
	}

}
