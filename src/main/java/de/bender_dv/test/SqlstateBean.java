/* 
 * File: SqlStateBean.java global Context AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.test;

import java.io.UnsupportedEncodingException;

import com.ibm.as400.access.AS400Bin4;
import com.ibm.as400.access.BinaryFieldDescription;
import com.ibm.as400.access.RecordFormat;

import de.bender_dv.as400.datastructure.AS400Record;
import de.bender_dv.as400.datastructure.DataStructure;

public class SqlstateBean extends DataStructure {
	private AS400Record record;
	private static RecordFormat format = new RecordFormat();
	static{
		format.addFieldDescription(new BinaryFieldDescription(new AS400Bin4(), "SQLSTATE"));
	}
	public SqlstateBean(){
		super(500);
		record = new AS400Record(format);
	}
	public SqlstateBean(int state){
		super(500);
		record = new AS400Record(format);
		setSQLSTATE(state);
	}
	public SqlstateBean(byte[] contents) throws UnsupportedEncodingException {
		super(500);
		record = new AS400Record(format, contents);
	}
	public void setRecord(byte[] rec) 
	throws UnsupportedEncodingException{
		record.setContents(rec);
	}
	public byte[] getRecord(){
		return record.getContents();
	}
	public int getSQLSTATE() {
		Integer result = null;
		result = (Integer) record.getField("SQLSTATE");
		return result.intValue();
	}

	public void setSQLSTATE(int sqlState) {
		try {
			record.setField("SQLSTATE", new Integer(sqlState));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}