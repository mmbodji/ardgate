/* 
 * File: AS400.java Example for usage AppServer4RPG
 * Copyright (c) 2008 Dieter Bender <db@bender-v.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.                                       
 *
 * This program is distributed in the hope that it will be useful,     
 * but WITHOUT ANY WARRANTY; without even the implied warranty of      
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
 * GNU General Public License for more details.                        
 *                                                                    
 * You should have received a copy of the GNU General Public License   
 * along with this program; if not, write to the Free Software         
 * Foundation, Inc., 59 Temple Place,                                  
 * Suite 330, Boston, MA  02111-1307   USA                             
 * You might find a version at http://www.gnu.org                      
 *  
 *=============================================================================  
 */
package de.bender_dv.test;

import java.io.UnsupportedEncodingException;

import com.ibm.as400.access.AS400Bin4;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.BinaryFieldDescription;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.RecordFormat;

import de.bender_dv.as400.datastructure.AS400Record;
import de.bender_dv.as400.datastructure.DataStructure;

/**
 * Hybrid Object to convert from PORDS to POJO
 * further on this should be replaced by generated objects, or Objects created by Reflection
 * based on some XML configuration or something else.
 * 
 * @author bender
 * 
 */
public class AS400Kunde extends DataStructure {

	/**
	 * 
	 */
	private AS400Record record;
	private static RecordFormat format = new RecordFormat();
	static{
		format.addFieldDescription(new BinaryFieldDescription(new AS400Bin4(), "KUNDE_ID"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(25, 273), "VORNAME"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(25, 273), "NACHNAME"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(25, 273), "STRASSE"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(6, 273), "PLZ"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(25, 273), "ORT"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(15, 273), "TEL"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(15, 273), "FAX"));
		format.addFieldDescription(new CharacterFieldDescription(new AS400Text(25, 273), "EMAIL"));
	}
	public AS400Kunde() throws UnsupportedEncodingException {
		super(500);
		record = new AS400Record(format);
	}
	public AS400Kunde(byte[] contents) throws UnsupportedEncodingException {
		super(500);
		record = new AS400Record(format, contents);
	}

	public void setKundeDS(byte[] kundeDS) 
	throws UnsupportedEncodingException{
		record.setContents(kundeDS);
	}
	public byte[] getKundeDS(){
		return record.getContents();
	}
	public String getEMAIL() {
		return (String) record.getField("EMAIL");
	}

	public void setEMAIL(String email) {
		try {
			record.setField("EMAIL", email);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getFAX() {
		return (String) record.getField("FAX");
	}

	public void setFAX(String fax) {
		try {
			record.setField("FAX", fax);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getKUNDE_ID() {
		Integer result = null;
		result = (Integer) record.getField("KUNDE_ID");
		return result.intValue();
	}

	public void setKUNDE_ID(int kunde) {
		try {
			record.setField("KUNDE_ID", new Integer(kunde));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getNACHNAME() {
		return (String) record.getField("NACHNAME");
	}

	public void setNACHNAME(String nachname) {
		try {
			record.setField("NACHNAME", nachname);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getORT() {
		return (String) record.getField("ORT");
	}

	public void setORT(String ort) {
		try {
			record.setField("ORT", ort);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getPLZ() {
		return (String) record.getField("PLZ");
	}

	public void setPLZ(String plz) {
		try {
			record.setField("PLZ", plz);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getSTRASSE() {
		return (String) record.getField("STRASSE");
	}

	public void setSTRASSE(String strasse) {
		try {
			record.setField("STRASSE", strasse);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getTEL() {
		return (String) record.getField("TEL");
	}

	public void setTEL(String tel) {
		try {
			record.setField("TEL", tel);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getVORNAME() {
		return (String) record.getField("VORNAME");
	}

	public void setVORNAME(String vorname) {
		try {
			record.setField("VORNAME", vorname);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
