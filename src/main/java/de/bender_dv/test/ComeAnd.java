package de.bender_dv.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ComeAnd {

	public static void run(String name, String[] parms) 
			throws Exception {
		Class myClass = Class.forName(name);
		Method myMethod = myClass.getMethod("main", new Class[] {String[].class});
		myMethod.invoke(null, new Object[]{parms});
		return;
	}
}
