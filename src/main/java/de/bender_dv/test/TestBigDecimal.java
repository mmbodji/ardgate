package de.bender_dv.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestBigDecimal {
	public static void main(String[] args)
	{
	    Random rnd = new Random();
	    List <Double> dl = new ArrayList <Double> ();
	    List <BigDecimal> bdl = new ArrayList <BigDecimal> ();

	    for(int i=0; i<1000; i++)
	    {
	        double d = rnd.nextDouble();
	        dl.add (d);
	        bdl.add (new BigDecimal (d));
	    }

	    long t1 = System.nanoTime();
	    double t;
	    for (double d1 : dl)
	            for (double d2 : dl)
	                t = d1 * d2;

	    long t2 = System.nanoTime();

	    for (BigDecimal b1 : bdl)
	        for (BigDecimal b2 : bdl)
	                b1.multiply (b2);

	    long t3 = System.nanoTime();

	    System.out.println (String.format ("%f", (t2 - t1) / 1e9));
	    System.out.println (String.format ("%f", (t3 - t2) / 1e9));
	    System.out.println (String.format ("%f", (double) (t3 - t2) / (double) (t2 - t1)));
	} 
}
